#!/bin/sh

rm -f Saves/*
mkdir -p Saves
rm -f states* population* ids*

NB_OF_GAMES=`grep "GAMES_NB" ../../include/*.hpp | cut -f 3`
STATE_PER_GAME=`grep "STATE_PER_GAME" ../../include/*.hpp | cut -f 2`

python << EOF > states
print("0 " * ($STATE_PER_GAME * $NB_OF_GAMES + 1) + "0")
EOF

echo "Reset done, genetic search configured with $NB_OF_GAMES games per ratios"
cat states
