#!/bin/bash

function DISPLAY {
	COUNT=-1
	while read -r line; do
		if [ ... ];then
			printf "%4d - %d  %d %-3d %-7d \033[32m%5d\033[0m | %d %-3d %-7d \033[32m%5d\033[0m | %d %-3d %-7d \033[32m%5d\033[0m  \033[33m%d\033[0m\n" $COUNT $line
		fi
		COUNT=$(( $COUNT + 1 ))
	done < states
}

 while true
do
	clear
	DISPLAY
	head -n 1 states
	sleep 0.5
done

