import numpy as np
import matplotlib.pyplot as plt
import os
import sys


fig, ax = plt.subplots()

fig.canvas.draw()

plt.title(os.path.basename(sys.argv[1]))
datas = np.genfromtxt(sys.argv[1], delimiter=" ")
dat = datas.reshape(datas.shape[1], datas.shape[0])
for col in datas:
    ax.plot(col)
labels = []
labels.append("free aligned 1")
labels.append("one aligned 1")
labels.append("cellnb")
labels.append("captures")
labels.append("capturable")
labels.append("five aligne")
labels.append("free aligne 2")
labels.append("secure one 2")
labels.append("free aligned 3")
labels.append("one aligne 3")
labels.append("free align 4")
labels.append("one align 4")
labels.append("double free 3")
labels.append("last turn")
labels.append("free aligned 1")
labels.append("one aligned 1")
labels.append("cellnb")
labels.append("captures")
labels.append("capturable")
labels.append("five aligne")
labels.append("free aligne 2")
labels.append("secure one 2")
labels.append("free aligned 3")
labels.append("one aligne 3")
labels.append("free align 4")
labels.append("one align 4")
labels.append("double free 3")
labels.append("last turn")

ax.plot([0 for i in range(len(labels))])
print(len(labels))

plt.xticks(range(0, len(labels)), labels, rotation='vertical')
ax.set_xticklabels(labels)
plt.show()

