#include "SaveManager.class.hpp"

SaveManager::SaveError::SaveError(const std::string & err): _error(err)
{

}

const char * SaveManager::SaveError::what(void) const noexcept
{
	std::string	s("Save Error: ");
	return (s + _error).c_str();
}

SaveManager::SaveManager(const std::string & file):
	_saveRepository(file)
{

}

SaveManager::~SaveManager(void)
{
	
}

std::vector<int>	SaveManager::loadAsIntVector(const std::string & file,
												 const unsigned int len)
{
	std::string		line;
	std::ifstream   ifs(_saveRepository + "/" + file);
	if (!ifs.is_open())
		throw SaveError(_saveRepository + "/" + file
						+ ": " + std::strerror(errno));
	std::getline(ifs, line);
	std::istringstream iss(line);
	std::vector<int> ret(std::istream_iterator<int>{iss}, {});
	if (len > 0 && ret.size() != len)
		throw SaveError("Loading " + _saveRepository + "/"
						+ file + ": file may be corrupted");
	return ret;
}

std::vector<std::vector<int> >	SaveManager::loadAsDoubleIntVector(
	const std::string & file, const unsigned int width, const unsigned int height)
{
	std::string		line;
	std::ifstream   ifs(_saveRepository + "/" + file);
	if (!ifs.is_open())
		throw SaveError(_saveRepository + "/" + file
						+ ": " + std::strerror(errno));
	std::vector<std::vector<int> >	ret;
	ret.reserve(height);
	while (std::getline(ifs, line))
	{
		std::istringstream iss(line);
		std::vector<int> tmp(std::istream_iterator<int>{iss}, {});
		if (tmp.size() != width)
			throw SaveError("Loading " + _saveRepository + "/" + file + ": "
							"file may be corrupted, invalid width");
		ret.push_back(tmp);
	}
	if (ret.size() != height)
		throw SaveError("Loading " + _saveRepository + "/" + file + ": "
						"file may be corrupted, invalid height");
	return ret;
}

std::string SaveManager::doubleVectorIntToStr(std::vector<std::vector<int> > v)
{
	std::stringstream ss;
	auto itLine = v.begin();
	for ( ;  itLine != v.end() ; itLine++)
	{
		auto it = itLine->begin();
		ss << *it;
		for (it++ ; it  < itLine->end() ; it++)
			ss << " " << *it;
		ss << std::endl;
	}
	return ss.str();
}

std::string	SaveManager::vectorIntToStr(std::vector<int> v)
{
	std::stringstream ss;
	if (!v.size())
		return "";
	auto it = v.begin();
	ss << *it;
	for (it++ ; it  < v.end() ; it++)
		ss << " " << *it;
	return ss.str();
}

void	SaveManager::copyFile(const std::string & src, const std::string & dst)
{
	std::ifstream  srcFile(_saveRepository + "/" + src, std::ios::binary);
	std::ofstream  dstFile(_saveRepository + "/" + dst,
						   std::ios::binary | std::ios::trunc);

	if (!srcFile.is_open())
		throw SaveError(strerror(errno));
	if (!dstFile.is_open())
	{
		srcFile.close();
		throw SaveError(strerror(errno));
	}
	dstFile << srcFile.rdbuf();
	dstFile.close();
	srcFile.close();
}
