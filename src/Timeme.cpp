#include <iostream>

#include <map>
#include <unistd.h>
#include <sys/time.h>

struct			s_timedata
{
	s_timedata(void) {
		this->nb_call = 0;
		total_time = 0;
	};
	size_t				nb_call;
	struct timeval		start;
	struct timeval		end;
	double				total_time;
};

static std::map<const char *, struct s_timedata>		timedatas;

namespace timeme {

	void			start(const char * fct)
	{
		if (timedatas.find(fct) == timedatas.end())
			timedatas[fct] = s_timedata();
		timedatas[fct].nb_call++;
		gettimeofday(&timedatas[fct].start, NULL);
	}

	void			end(const char * fct)
	{
		if (timedatas.find(fct) == timedatas.end())
		{
			std::cerr << "Missing start point" << std::endl;
			return ;
		}
		gettimeofday(&timedatas[fct].end, NULL);
		time_t sec = timedatas[fct].end.tv_sec - timedatas[fct].start.tv_sec;
		suseconds_t usec = timedatas[fct].end.tv_usec - timedatas[fct].start.tv_usec;
		timedatas[fct].total_time += (sec + usec / 1000000.0);
	};

	static void dump(const char *fct, struct s_timedata datas)
	{
		std::cout << "======== " << fct << " ========"<< std::endl;
		std::cout << "nb_calls: " << datas.nb_call << std::endl;
		std::cout << "total time: " << datas.total_time << std::endl;
		std::cout << "average time: " << datas.total_time / datas.nb_call << std::endl;
	}
	void			report(void)
	{
		for (auto it = timedatas.cbegin(); it != timedatas.cend(); ++it)
			dump((*it).first, (*it).second);
	}

	void			clear(void)
	{
		timedatas.erase(timedatas.begin(), timedatas.end());
	}
};
