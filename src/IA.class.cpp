/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IA.class.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/11 18:48:53 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/28 16:38:16 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <IA.class.hpp>
#include "Game.class.hpp"
#include "Timeme.hpp"
//#define DEBUG_H(x)
#define DEBUG_H(x) //std::cout << x << std::endl;
#define DEBUG_AI(x) //std::cout << x << std::endl;
#define DEBUG_TH(x) //std::cout << x << std::endl;

bool			comp(struct s_data_heur lhs, struct s_data_heur rhs)
{
	size_t			i = 0;

	while (i < 22)
	{
		if (lhs.serie[i] != rhs.serie[i])
			return (lhs.serie[i] < rhs.serie[i]);
		i++;
	}
	return (lhs.serie[i] < rhs.serie[i]);
}

IA::IA(const char * name, enum e_player id, size_t depth) :
	Player(name, id),
	_heuristic_values(&comp),
	_depth(depth),
	_idToPlay(0),
	_ct(0),
	_cut(0),
	_turn(),
	_done(false),
	_hRatios(GeneticSolver::instance().getDefaultRatios())
{
}

IA::IA(const char * name, enum e_player id, size_t depth, const int *ratios) :
	Player(name, id),
	_heuristic_values(&comp),
	_depth(depth),
	_idToPlay(0),
	_ct(0),
	_cut(0),
	_turn(),
	_done(false),
	_hRatios(ratios)
{
}

IA::~IA(void)
{
	if (_turn.joinable())
		_turn.join();
}

void			IA::_launchPlay(Game & game)
{
	Board &		board = game.getBoard();
	int			test = 0;

	_ct = 0;
	_cut = 0;
	if (board.getMovesNb() == 0)
		_idToPlay = CELL_NUMBER / 2 + std::rand() % 5 - 3 + (std::rand() % 5 - 3) * AREA_SIZE;
	else
		test = _alphabeta(game, board, _depth, MIN_INT, MAX_INT);
	DEBUG_AI(test);
	test++;
	_done = true;
	DEBUG_TH("END OF THREAD !!!!");
}

void			IA::play(Game & game)
{
	_game = &game;
	game.disableBoardEvents();
	_turn = std::thread(&IA::_launchPlay, this, std::ref(game));
}

bool			IA::cancel(void)
{
	DEBUG_TH("In Concel !!");
	if (_cancel.try_lock())
	{
		DEBUG_TH("In Concel !!");
		_done = true;
		if (_turn.joinable())
			_turn.join();
		_done = false;
		DEBUG_TH("In Concel  unlock!!");
		_cancel.unlock();
		return (true);
	}
	else
		return (false);
}

void			IA::quit(void)
{
	_done = true;
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

void	IA::update(void)
{
	if (_done)
	{
		DEBUG_TH("In Update after join");
		DEBUG_TH("IA explored: " << _ct);
		DEBUG_TH("IA cuted: " << _cut);
		DEBUG_TH("IA WANTS TO PLAY in: " << _idToPlay);
		if (_turn.joinable())
			_turn.join();
		_done = false;
		_game->playTo(_idToPlay);
	}
}

void	IA::_shell_sort(int tab[CELL_NUMBER][2], int ct)
{
	static int	gaps[] = {301, 132, 57, 23, 10, 4, 1};
	int			tmp[2];
	int			gap;
	int			k;

	for (int i = 0 ; i < 7 ; i++)
	{
		gap = gaps[i];
		for (int j = gap ; j < ct ; j++)
		{
			tmp[0] = tab[j][0];
			tmp[1] = tab[j][1];
			for (k = j ; k >= gap && tab[k - gap][0] < tmp[0] ; k -= gap)
			{
				tab[k][0] = tab[k - gap][0];
				tab[k][1] = tab[k - gap][1];
			}
			tab[k][0] = tmp[0];
			tab[k][1] = tmp[1];
		}
	}
}

int		IA::_getHeuristics(Board & board, const PlayerData & current,
						   int tab[CELL_NUMBER][2])
{
	const Area	area = board.getPlayableArea();
	const int	size = area.size();
	int				heur;
	int				ct = 0;
	int				pos;
	/* struct s_data_heur		datas; */
	
	for (int i = 0 ; i < size ; i++)
	{
		pos = area[i];
		if (!board.canPlayTo(pos, current.color))
			continue;
		/* timeme::start("preconf"); */
		/* std::memcpy(datas.serie, board.serialize_at(pos, current.color), sizeof(int) * 23); */
		/* const auto res = _heuristic_values.find(datas); */
		/* timeme::end("preconf"); */
		/* if (res != _heuristic_values.end() && (*res).player == current.id) */
		/* { */
			/* heur = (*res).heur; */
		/* } */
		/* else */
		/* { */
			board.silentPlayTo(pos, current.color);
			if (board.has_win())
				heur = WIN_VALUE;
			else
				heur = this->heuristic(board, current);
			/* datas.heur = heur; */

			/* timeme::start("insert"); */
			/* _heuristic_values.insert(s_data_heur(board.serialize(), heur, current.id)); */
			/* timeme::end("insert"); */
			board.switch_player();
			board.silentUnplayLast();
			board.switch_player();
		/* } */
		/* TODO Dabord find selon la serialization
		 * else on calcule normale, puis on inserere
		 * */
		/* TODO
		 * Calcul avec/sans capture
		 * */
		/*TODO CONTINUE HERE */
		tab[ct][0] = heur;
		tab[ct][1] = pos;
		ct++;
	}
	_shell_sort(tab, ct);
	return (ct);
}

int			IA::_alphabeta(Game & game, Board & board, int depth,
						   int alpha, int beta)
{
	const PlayerData &	currentPlayer = board.currentPlayer();
	int					heuristics[CELL_NUMBER][2];
	int					best_value = 0;
	int					i;
	int					value;
	int					playId;
	int					check;

	/* unsigned int			*d = board.serialize(); */
	/* for (int i = 0; i < 23; i++) */
	/* 	std::cout << d[i] << " "; */
	/* std::cout << std::endl; */

	if (_done == true)
		return (0);
	_ct++;
	if (board.has_win())
		return (WIN_VALUE);
	const int	maxDepth = _depth;
	/* const float ratio = 1.0 - (maxDepth - depth + 1.1) / 10; */
	/* const float ratio = 1.0 - ((maxDepth - depth) <= 3 ? (maxDepth - depth) * 0.1 : 0.80); */
	const float ratio = 1.0 - ((maxDepth - depth) <= 3 ? (maxDepth - depth + 1.1) / 10 : 0.8);
	if (depth == 0)
	{
		if (maxDepth % 2 == 0)
			return (this->heuristic(board, currentPlayer));
		else
			return (this->heuristic(board, board.getPlayerData(
					currentPlayer.id == _PLAYER1 ? _PLAYER2 : _PLAYER1)));
	}
	if (currentPlayer.id == _id)
	{
		check = _getHeuristics(board, currentPlayer, heuristics) * ratio + 1;
		best_value = MIN_INT;
		for (i = 0; i < check ; i++)
		{
			playId = heuristics[i][1];
			if (heuristics[i][0] >= WIN_VALUE)
			{
				DEBUG_AI("WON BLACK");
				if (depth == maxDepth)
					_idToPlay = playId;
				return WIN_VALUE;
			}
			else
			{
				board.silentPlayTo(playId, currentPlayer.color);
				board.switch_player();
				value = _alphabeta(game, board, depth - 1, alpha, beta);
				board.silentUnplayLast();
				board.switch_player();
			}
			if (value > best_value)
			{
				best_value = value;
				if (depth == maxDepth)
				{
					DEBUG_H(playId << " set top");
					_idToPlay = playId;
				}
			}
			if (best_value > alpha)
				alpha = best_value;
			if (best_value >= beta)
			{
				_cut++;
				return best_value;
			}
		}
		return (best_value);
	}
	else
	{
		check = _getHeuristics(board, currentPlayer, heuristics) * ratio;
		best_value = MAX_INT;
		for (i = 0; i < check ; i++)
		{
			playId = heuristics[i][1];
			if (!board.canPlayTo(playId, currentPlayer.color))
				continue;
			if (heuristics[i][0] >= WIN_VALUE)
			{
				DEBUG_AI("WON WHITE IN" << heuristics[i][1]);
				return -WIN_VALUE;
			}
			else
			{
				board.silentPlayTo(playId, currentPlayer.color);
				board.switch_player();
				value = _alphabeta(game, board, depth - 1, alpha, beta);
				board.silentUnplayLast();
				board.switch_player();
			}
			if (value < best_value)
			{
				best_value = value;
			}
			if (best_value < beta)
				beta = best_value;
			if (best_value <= alpha)
			{
				_cut++;
				return best_value;
			}
		}
		return (best_value);
	}
}

int				IA::heuristic(const Board & board, const PlayerData & playerDatas)
{
	float		pVal = 0;
	const int *	hRatios = _hRatios;

	const PlayerData & opposite = board.getPlayerData(
		playerDatas.id == _PLAYER1 ? _PLAYER2 : _PLAYER1);

	if (playerDatas.oneAligneds[0] && opposite.oneAligneds[0])
	{
		pVal += hRatios[0] * playerDatas.freeAligneds[0];
		pVal += hRatios[1] * playerDatas.oneAligneds[0];
	}

	pVal += hRatios[2] * playerDatas.cellNb;
	pVal += hRatios[3] * playerDatas.captures;
	pVal -= hRatios[4] * playerDatas.capturables;
	pVal += hRatios[5] * playerDatas.fiveAligneds;
	pVal += hRatios[6] * playerDatas.freeAligneds[1];
	pVal -= hRatios[7] * (playerDatas.oneAligneds[1] - playerDatas.capturables);
	pVal += hRatios[8] * playerDatas.freeAligneds[2];
	pVal += hRatios[9] * playerDatas.oneAligneds[2];
	pVal += hRatios[10] * playerDatas.freeAligneds[3];
	pVal += hRatios[11] * playerDatas.oneAligneds[3];
	/* pVal += hRatios[12] * playerDatas.doubleFreeThree.size(); */
	pVal += hRatios[13] * playerDatas.lastTurn;

	if (opposite.oneAligneds[0] && opposite.oneAligneds[1])
	{
		pVal -= hRatios[14] * opposite.freeAligneds[0];
		pVal -= hRatios[15] * opposite.oneAligneds[0];
	}

	pVal -= hRatios[16] * opposite.cellNb;
	pVal -= hRatios[17] * opposite.captures;
	pVal += hRatios[18] * opposite.capturables;
	pVal -= hRatios[19] * opposite.fiveAligneds;
	pVal -= hRatios[20] * opposite.freeAligneds[1];
	pVal -= hRatios[21] * (opposite.oneAligneds[1] - opposite.capturables);
	pVal -= hRatios[22] * opposite.freeAligneds[2];
	pVal -= hRatios[23] * opposite.oneAligneds[2];
	pVal -= hRatios[24] * opposite.freeAligneds[3];
	pVal -= hRatios[25] * opposite.oneAligneds[3];
	/* pVal -= hRatios[26] * opposite.doubleFreeThree.size(); */
	pVal += hRatios[27] * playerDatas.lastTurn;

	return pVal;
}

int				IA::_minimax(Game & game, Board & board, int depth)
{
	_ct++;
	const PlayerData &	currentPlayer = board.currentPlayer();
	int					best_value = 0;
	int					i;
	int					value;

	_ct++;
	if (board.has_win())
		return (500000);
	if (depth == 0)
		return (this->heuristic(board, currentPlayer));
	if (currentPlayer.id == _id)
	{
		best_value = MIN_INT;
		for (i = 0; i < CELL_NUMBER; i++)
		{
			if (!board.canPlayTo(i, currentPlayer.color))
				continue;
			board.silentPlayTo(i, currentPlayer.color);
			board.switch_player();
			value = _minimax(game, board, depth - 1);
			board.silentUnplayLast();
			board.switch_player();
			if (value > best_value)
			{
				_idToPlay = i;
				best_value = value;
			}
		}
		return (best_value);
	}
	else
	{
		best_value = MAX_INT;
		for (i = 0; i < CELL_NUMBER; i++)
		{
			if (!board.canPlayTo(i, currentPlayer.color))
				continue;
			board.silentPlayTo(i, currentPlayer.color);
			board.switch_player();
			value = _minimax(game, board, depth - 1);
			board.silentUnplayLast();
			board.switch_player();
			if (value < best_value)
			{
				_idToPlay = i;
				best_value = value;
			}
		}
		return (best_value);
	}
}
