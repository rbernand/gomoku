/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Board.class.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/11 14:09:38 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/28 13:31:49 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Board.class.hpp"
#include "Game.class.hpp"
#include "Timeme.hpp"


Board::Move::Move(int p, std::vector<int> c, Area & a,
		PlayerData & p1, PlayerData & p2) :
	pos(p),
	captures(c),
	area(a),
	p1Data(p1),
	p2Data(p2)
{
}

Board::Board(Game & game):
	_board(),
	_game(game),
	_currentPlayer(_NONE),
	_p1Data(_PLAYER1),
	_p2Data(_PLAYER2),
	_currentPData(NULL),
	_oppositePData(NULL),
	_star(),
	_starCenter(),
	_lastCaptured(),
	_xarea{AREA_SIZE, 0},
	_yarea{AREA_SIZE, 0},
	_area(),
	_moves()
{
	for (int i = 0 ; i < CELL_NUMBER ; ++i)
	{
		_board[i].id = i;
		_board[i].cell = _EMPTY;
	}
	this->_init_star();
	this->_init_star_centers();
	_lastCaptured.reserve(16);
	_area.reserve(CELL_NUMBER);
	_moves.reserve(200);
}

Board::~Board()
{
}

const Board::Node *	Board::getBoard(void) const
{
	return _board;
}

enum e_cell		Board::get(size_t pos) const
{
	return _board[pos].cell;
}

enum e_cell		Board::get(size_t pos_x, size_t pos_y) const
{
	return (_board[pos_y * AREA_SIZE + pos_x].cell);
}

void			Board::playTo(size_t where, enum e_cell who)
{
	// TODO Here check for free / capture.. / update datas for heuristik
	int		captures;

	this->set(where, who);
	if (_moves.size() == 0)
	{
		_initSearchArea();
		_update_search_area(where);
	}
	else if (isInSearchArea(where))
		_update_search_area(where);
	this->update_alignments(where);
	if ((captures = this->capture(where)))
	{
		this->currentPlayer().captures += captures;
		_game.updateCaptures(this->currentPlayer());
	}
	auto it = _lastCaptured.begin();
	while (it != _lastCaptured.end())
	{
		_game.updatePlate(*it, _EMPTY);
		it++;
	}
	_game.updatePlate(where, who);
	_currentPData->cellNb++;
	/* this->updateDoubleThree(where, this->currentPlayer().color); */
	Move test = Move(_last, std::vector<int>(_lastCaptured), _area, _p1Data, _p2Data);
	//	print_move(test);
	_moves.push_back(test);
	_last = -1;
	_lastCaptured.clear();
}

unsigned int	*Board::serialize_at(size_t where, enum e_cell who)
{
	unsigned int		*serie;

	_board[where].cell = who;
	this->capture(where);
	serie = this->serialize();
	_board[where].cell = _EMPTY;
	for (auto it = _lastCaptured.begin(); it != _lastCaptured.end(); it++)
		_board[*it].cell = who == _BLACK ? _WHITE : _BLACK;
	_lastCaptured.clear();
	return (serie);
}

unsigned int			*Board::serialize(void) const
{
	static unsigned int		outstr[23];
	int				value;
	size_t			i = 0;

	value = 0;
	while (i < CELL_NUMBER)
	{
		value += 2 - _board[i].cell;
		i++;
		if (i % 16 == 0)
		{
			outstr[i / 16 - 1] = value;
			value = 0;
		}
		value = value << 2;
	}
	outstr[22] = value;
	return (outstr);
}

void			Board::unplayLast(void)
{
	if (!_moves.size())
	{
		std::cout << "NO MOVES TO UNDO" << std::endl;
		return;
	}
	Move &	lastMove = _moves.back();
	int		id;

	_unset(lastMove.pos);
	_game.updatePlate(lastMove.pos, _EMPTY);
	auto	it = lastMove.captures.begin();
	while (it != lastMove.captures.end())
	{
		id = *it;
		_set(*it, _currentPData->color);
		_game.updatePlate(id, _currentPData->color);
		it++;
	}
	_game.updateCaptures(*_oppositePData);
	_game.updateCaptures(*_currentPData);
	_moves.pop_back();
	if (_moves.size() == 0)
	{
		_p1Data = PlayerData(_p1Data.id);
		_p2Data = PlayerData(_p2Data.id);
		return;
	}
	Move & lastMove1 = _moves.back();
	_p1Data = lastMove1.p1Data;
	_p2Data = lastMove1.p2Data;
	_area = lastMove1.area;
}

bool			Board::canPlayTo(size_t where, enum e_cell who)
{
	const Node &	node = _board[where];

	return (node.cell == _EMPTY && countFreeThree(where, who) < 2);
}

void			Board::silentPlayTo(size_t where, enum e_cell who)
{
	int				captures;

	this->set(where, who);
	if (_moves.size() == 0)
	{
		_initSearchArea();
		_update_search_area(where);
	}
	else if (isInSearchArea(where))
		_update_search_area(where);
	this->update_alignments(where);
	if ((captures = this->capture(where)))
		this->currentPlayer().captures += captures;
	_currentPData->cellNb++;
	_moves.push_back(Move(_last, std::vector<int>(_lastCaptured),
				_area, _p1Data, _p2Data));
	_last = -1;
	_lastCaptured.clear();
}

void			Board::silentUnplayLast(void)
{
	Move &	lastMove = _moves.back();

	_unset(lastMove.pos);
	_area = lastMove.area;
	auto	it = lastMove.captures.begin();
	while (it != lastMove.captures.end())
	{
		_set(*it, _currentPData->color);
		it++;
	}
	_p1Data = lastMove.p1Data;
	_p2Data = lastMove.p2Data;
	_moves.pop_back();
	if (_moves.size() == 0)
	{
		_p1Data = PlayerData(_p1Data.id);
		_p2Data = PlayerData(_p2Data.id);
		return;
	}
	Move & lastMove1 = _moves.back();
	_p1Data = lastMove1.p1Data;
	_p2Data = lastMove1.p2Data;
	_area = lastMove1.area;
}

int				Board::_align_count_left(int center, const Branch & branch,
		e_cell current)
{
	int		count = 0;
	for (int j = center - 1 ; j >= 0 && branch[j]->cell == current ; j--)
		count++;
	return count;
}

int				Board::_align_count_right(int center, const Branch & branch,
		e_cell current)
{
	int		count = 0;
	int		size = branch.size();
	for (int j = center + 1 ; j < size && branch[j]->cell == current ; j++)
		count++;
	return count;
}

std::vector<Board::Move> Board::getMoves(void) const
{
	return (_moves);
}

void			Board::_align_update_value(int x, int sub, int nb, int * tab)
{
	if (sub > 0 && x > 0 && x < 5)
	{
		tab[x - 1] += nb;
	}
}

void			Board::_update_oneAligned(int x, int nb, PlayerData & datas,
		int border, int max)
{
	if (x == 2)
	{
		if (max && border < max)
			datas.capturables += nb;
		else if (!max && border >= 0)
			datas.capturables += nb;
	}
	_align_update_value(x, x, nb, datas.oneAligneds);
}

void			Board::supress_alignments(size_t pos, e_cell supressed)
{
	e_cell		opposite = supressed == _WHITE ? _BLACK : _WHITE;
	int			left;
	int			right;
	Star &		star = _star[pos];
	Centers	&	centers = _starCenter[pos];
	int			x;
	int			i;

	auto	update_supressed_on_branch = [&] (int center, const Branch & branch) {
		int		size = branch.size();
		int		lpos = center - left - 1;
		int		rpos = center + right + 1;
		e_cell	l = lpos < 0 ? opposite : branch[lpos]->cell;
		e_cell	r = rpos >= size ? opposite : branch[rpos]->cell;

		if (x >= 4)
			this->_delete_five_aligns(branch[center]->id, i, *_oppositePData);
		if (l == _EMPTY && r == _EMPTY)
		{
			if (x < 4)
				_oppositePData->freeAligneds[x] -= 2;
			_align_update_value(left, left, 2, _oppositePData->freeAligneds);
			_align_update_value(right, right, 2, _oppositePData->freeAligneds);
		}
		else if (l == opposite && r == opposite)
		{
			if (left < 5)
				_update_oneAligned(left, 1, *_oppositePData, lpos, 0);
			if (right < 5)
				_update_oneAligned(right, 1, *_oppositePData, rpos, size);
		}
		else
		{
			if (l == _EMPTY)
			{
				if (x < 4)
					_update_oneAligned(x + 1, -1, *_oppositePData, rpos, size);
				if (right < 5)
					_update_oneAligned(right, 1, *_oppositePData, rpos, size);
				_align_update_value(left, left, 2, _oppositePData->freeAligneds);
			}
			else
			{
				if (x < 4)
					_update_oneAligned(x + 1, -1, *_oppositePData, lpos, 0);
				if (left < 5)
					_update_oneAligned(left, 1, *_oppositePData, lpos, 0);
				_align_update_value(right, right, 2, _oppositePData->freeAligneds);
			}
		}
	};

	auto	update_opposites_on_branch = [&] (int center, const Branch & branch) {
		int		size = branch.size();
		int		lpos = center - left - 1;
		int		rpos = center + right + 1;
		e_cell	l = lpos < 0 ? supressed : branch[lpos]->cell;
		e_cell	r = rpos >= size ? supressed : branch[rpos]->cell;

		if (left && l == _EMPTY)
		{
			_update_oneAligned(left, -1, *_currentPData, 1, 0);
			_align_update_value(left, left, 2, _currentPData->freeAligneds);
		}
		else if (left < 5 && left && l != _EMPTY)
		{
			_update_oneAligned(left, 1, *_currentPData, lpos, 0);
		}
		if (right && r == _EMPTY)
		{
			_update_oneAligned(right, -1, *_currentPData, 1, 0);
			_align_update_value(right, right, 2, _currentPData->freeAligneds);
		}
		else if (right < 5 && right && r != _EMPTY)
		{
			_update_oneAligned(right, 1, *_currentPData, rpos, size);
		}
	};

	for (i = 0 ; i < 4 ; i++)
	{
		left = _align_count_left(centers[i], star[i], supressed);
		right = _align_count_right(centers[i], star[i], supressed);
		x = left + right;
		update_supressed_on_branch(centers[i], star[i]);
		left = _align_count_left(centers[i], star[i], opposite);
		right = _align_count_right(centers[i], star[i], opposite);
		x = left + right;
		update_opposites_on_branch(centers[i], star[i]);
	}
}

void			Board::update_alignments(size_t pos)
{
	e_cell		current = this->currentPlayer().color;
	e_cell		opposite = current == _WHITE ? _BLACK : _WHITE;
	int			left;
	int			right;
	Star &		star = _star[pos];
	Centers	&	centers = _starCenter[pos];
	int			x;
	int			i;

	auto	update_currents_on_branch = [&] (int center, const Branch & branch) {
		int		size = branch.size();
		int		lpos = center - left - 1;
		int		rpos = center + right + 1;
		e_cell	l = lpos < 0 ? opposite : branch[lpos]->cell;
		e_cell	r = rpos >= size ? opposite : branch[rpos]->cell;

		if (x >= 4)
			this->_store_five_aligns(branch[center]->id, i, *_currentPData);
		if (l == _EMPTY && r == _EMPTY)
		{
			if (x < 4)
				_currentPData->freeAligneds[x] += 2;
			_align_update_value(left, left, -2, _currentPData->freeAligneds);
			_align_update_value(right, right, -2, _currentPData->freeAligneds);
		}
		else if (l == opposite && r == opposite)
		{
			if (left < 5)
				_update_oneAligned(left, -1, *_currentPData, lpos, 0);
			if (right < 5)
				_update_oneAligned(right, -1, *_currentPData, rpos, size);
		}
		else
		{
			if (l == opposite)
			{
				if (x < 4)
					_update_oneAligned(x + 1, 1, *_currentPData, lpos, 0);
				if (left < 5)
					_update_oneAligned(left, -1, *_currentPData, lpos, 0);
				_align_update_value(right, right, -2,
						_currentPData->freeAligneds);
			}
			else
			{
				if (x < 4)
					_update_oneAligned(x + 1, 1, *_currentPData, rpos, size);
				if (right < 5)
					_update_oneAligned(right, -1, *_currentPData, rpos, size);
				_align_update_value(left, left, -2, _currentPData->freeAligneds);
			}
		}
	};

	auto update_opposites_on_branch = [&] (int center, const Branch & branch) {
		int		size = branch.size();
		int		lpos = center - left - 1;
		int		rpos = center + right + 1;
		e_cell	l = lpos < 0 ? current : branch[lpos]->cell;
		e_cell	r = rpos >= size ? current : branch[rpos]->cell;

		if (left < 0)
			std::cout << "pponey " << std::endl;
		if (left > 0 && left < 5)
		{
			if (l == _EMPTY)
			{
				_align_update_value(left, left, -2, _oppositePData->freeAligneds);
				_update_oneAligned(left, 1, *_oppositePData, 1, 0);
			}
			else if (l == current)
			{
				_update_oneAligned(left, -1, *_oppositePData, lpos, 0);
			}
		}
		if (right && right < 5)
		{
			if (r == _EMPTY)
			{
				_align_update_value(right, right, -2, _oppositePData->freeAligneds);
				_update_oneAligned(right, 1, *_oppositePData, 1, 0);
			}
			else if (r == current)
			{
				_update_oneAligned(right, -1, *_oppositePData, rpos, size);
			}
		}
	};

	for (i = 0 ; i < 4 ; i++)
	{
		const int center = centers[i];
		const Branch & branch = star[i];
		left = _align_count_left(center, branch, current);
		right = _align_count_right(center, branch, current);
		x = left + right;
		update_currents_on_branch(center, branch);
		left = _align_count_left(center, branch, opposite);
		right = _align_count_right(center, branch, opposite);
		update_opposites_on_branch(center, branch);
	}
}

int				Board::capture(size_t id)
{
	e_cell		current = this->currentPlayer().color;
	e_cell		opposite = current == _WHITE ? _BLACK : _WHITE;
	int			capture = 0;
	Star &		star = _star[id];
	Centers	&	centers = _starCenter[id];
	int			pos;

	auto	disable = [&] (int id) {
		this->unset(id);
		_lastCaptured.push_back(id);
	};

	for (int i = 0 ; i < 4 ; i++)
	{
		pos = centers[i] - 3;
		if (pos >= 0 && star[i][pos]->cell == current
				&& star[i][pos + 1]->cell == opposite
				&& star[i][pos + 2]->cell == opposite)
		{
			disable(star[i][pos + 1]->id);
			disable(star[i][pos + 2]->id);
			capture++;
		}
		pos = centers[i] + 3;
		if ((size_t)pos < star[i].size() && star[i][pos]->cell == current
				&& star[i][pos - 1]->cell == opposite
				&& star[i][pos - 2]->cell == opposite)
		{
			disable(star[i][pos - 1]->id);
			disable(star[i][pos - 2]->id);
			capture++;
		}
	}
	return capture;
}

void			Board::capture_void(size_t id)
{
	e_cell		current = this->currentPlayer().color;
	e_cell		opposite = current == _WHITE ? _BLACK : _WHITE;
	Star &		star = _star[id];
	Centers	&	centers = _starCenter[id];
	int			pos;

	auto	disable = [&] (int id) {
		_board[id].cell = _EMPTY;
		_lastCaptured.push_back(id);
	};

	for (int i = 0 ; i < 4 ; i++)
	{
		pos = centers[i] - 3;
		if (pos >= 0 && star[i][pos]->cell == current
				&& star[i][pos + 1]->cell == opposite
				&& star[i][pos + 2]->cell == opposite)
		{
			disable(star[i][pos + 1]->id);
			disable(star[i][pos + 2]->id);
		}
		pos = centers[i] + 3;
		if ((size_t)pos < star[i].size() && star[i][pos]->cell == current
				&& star[i][pos - 1]->cell == opposite
				&& star[i][pos - 2]->cell == opposite)
		{
			disable(star[i][pos - 1]->id);
			disable(star[i][pos - 2]->id);
		}
	}
}

// TODO -> OPTI may stop search if opposite color is present)
int				Board::countFreeThree(int id, e_cell current)
{
	/*
	 * TODO i should be private...
	 */
	Star &		star = _star[id];
	Centers	&	centers = _starCenter[id];
	int			freeThree = 0;
	int			size;
	int			center;

	auto		hasFreeThree = [&] (const int & min, const int & max,
			const Branch & branch) {
		int		counts[3] = {0, 0, 0};
		if (min < 0 || max >= size)
			return false;
		for (int i = min + 1 ; i < max ; i++)
			counts[branch[i]->cell]++;
		if (counts[current] == 2 && counts[_EMPTY] == 2
				&& branch[min]->cell == _EMPTY && branch[max]->cell == _EMPTY)
			return true;
		return false;
	};

	for (int i = 0 ; i < 4 ; i++)
	{
		size = star[i].size();
		center = centers[i];
		if (hasFreeThree(center - 4, center + 1, star[i]) ||
				hasFreeThree(center - 1, center + 4, star[i]) ||
				hasFreeThree(center - 2, center + 3, star[i]) ||
				hasFreeThree(center - 3, center + 2, star[i]))
			freeThree++;
	}
	return freeThree;
}

/* void			Board::updateDoubleThree(const int & id, e_cell current) */
/* { */
/* 	 *1/ */
/* 	e_cell      opposite = current == _WHITE ? _BLACK : _WHITE; */
/* 	Star &		star = _star[id]; */
/* 	Centers	&	centers = _starCenter[id]; */
/* 	int			freeNb; */

/* 	auto		checkBranch = [&] (const int & start, */
/* 								   const Branch & branch, const int & nb) { */
/* 		Node *	node; */
/* 		int		size = branch.size(); */
/* 		int		pos; */

/* 		for (int i = 0 ; i < nb ; i++) */
/* 		{ */
/* 			pos = start + i; */
/* 			if (pos < 0 || pos >= size) */
/* 				continue; */
/* 			node = branch[pos]; */
/* 			if (node->cell != _EMPTY) */
/* 				continue; */
/* 			if (node->forbid[opposite] */
/* 				&& (freeNb = countFreeThree(node->id, opposite)) < 2) */
/* 				this->allow(*node, opposite); */
/* 			freeNb = countFreeThree(node->id, current); */
/* 			if (node->forbid[current]) */
/* 			{ */
/* 				if (freeNb < 2) */
/* 					this->allow(*node, current); */
/* 			} */
/* 			else */
/* 			{ */
/* 				if (freeNb >= 2) */
/* 					this->forbid(*node, current); */
/* 			} */
/* 		} */
/* 	}; */

/* 	for (int i = 0 ; i < 4 ; i++) */
/* 	{ */
/* 		checkBranch(centers[i] - 4, star[i], 9); */
/* 	} */
/* } */

bool			Board::isInBoard(int pos)
{
	if (pos >= CELL_NUMBER || pos < 0)
		return (false);
	return (true);
}

void			Board::_store_five_aligns(int pos, int branch, PlayerData & datas)
{
	static const int	incr[4][4] = {{-1, 0, 1, 0}, {0, -19, 0, 19},
		{-1, -19, 1, 19}, {-1, 19, 1, -19}};
	int		check;
	int		inc;

	inc = incr[branch][0] + incr[branch][1];
	while (this->isInBoard(pos) && _board[pos].cell == datas.color)
		pos += inc;
	inc = incr[branch][2] + incr[branch][3];
	pos += inc;
	check = pos + inc * 4;
	while (this->isInBoard(check) && _board[check].cell == datas.color)
	{
		datas.store_five_align(pos, branch);
		pos += inc;
		check += inc;
	}
}

void			Board::_delete_five_aligns(int pos, int branch,
		PlayerData & datas)
{
	static int	incr[4][4] = {{-1, 0, 1, 0}, {0, -19, 0, 19},
		{-1, -19, 1, 19}, {-1, 19, 1, -19}};
	int		i = 0;
	int		inc;

	inc = incr[branch][0] + incr[branch][1];
	datas.delete_five_align(pos, branch);
	pos += inc;
	while (i < 4 && this->isInBoard(pos) && _board[pos].cell == datas.color)
	{
		datas.delete_five_align(pos, branch);
		i++;
		pos += inc;
	}

}

bool			Board::_can_be_captured(int id) const
{
	e_cell				current = this->currentPlayer().color;
	e_cell				opposite = current == _WHITE ? _BLACK : _WHITE;
	const Star &		star = _star[id];
	const Centers	&	centers = _starCenter[id];
	int					size;
	int					center;

	for (int i = 0 ; i < 4 ; i++)
	{
		center = centers[i];
		size = star[i].size();
		if (center - 1 >= 0 && center + 2 < size
				&& star[i][center + 1]->cell == current
				&& ((star[i][center - 1]->cell == opposite
						&& star[i][center + 2]->cell == _EMPTY)
					|| (star[i][center - 1]->cell == _EMPTY
						&& star[i][center + 2]->cell == opposite)))
		{
			return (true);
		}
		if (center - 2 >= 0 && center + 1 < size
				&& star[i][center - 1]->cell == current
				&& ((star[i][center + 1]->cell == opposite
						&& star[i][center - 2]->cell == _EMPTY)
					|| (star[i][center + 1]->cell == _EMPTY
						&& star[i][center - 2]->cell == opposite)))
		{
			return (true);
		}
	}
	return (false);
}

bool			Board::_five_align_breakable(void) const
{
	const FiveAlign &	al = _currentPData->fiveStorage;
	e_cell				current = this->currentPlayer().color;
	int					center;
	bool				capture_found = false;
	int					size;
	int					ct;
	int					i;

	for (auto val: al)
	{
		const std::pair<int, int> & x = val.second;
		const Branch & branch = _star[x.first][x.second];
		center = _starCenter[x.first][x.second];
		ct = 0;
		for (i = center ; i >= 0 && ct < 5 && branch[i]->cell == current ; i--)
		{
			ct++;
			if (_can_be_captured(branch[i]->id))
				capture_found = true;
		}
		size = branch.size();
		ct = 0;
		for (i = center ; i < size && ct < 5 && branch[i]->cell == current ; i++)
		{
			ct++;
			if (_can_be_captured(branch[i]->id))
				capture_found = true;
		}
		if (!capture_found)
			return (false);
	}
	return (true);
}

bool			Board::has_win(void) const
{
	PlayerData *	d = _currentPData;

	if (d->captures >= 5)
	{
		return (true);
	}
	if (d->fiveAligneds > 0)
	{
		if (d->lastTurn)
			return true;
		if (d->capturables > 0)
		{
			if (this->_oppositePData->captures == 4 || _five_align_breakable())
			{
				d->lastTurn = true;
				return (false);
			}
		}
		return (true);
	}
	return (false);
}

size_t			Board::getLast(void) const
{
	return (_last);
}

void			Board::setPlayer(enum e_player player)
{
	if (player != _PLAYER1 && player != _PLAYER2)
		throw Board::NoPlayer();
	_currentPlayer = player;
	if (player == _PLAYER1)
	{
		_oppositePData = &_p2Data;
		_currentPData = &_p1Data;
	}
	else
	{
		_oppositePData = &_p1Data;
		_currentPData = &_p2Data;
	}
}

int				Board::getMovesNb(void) const
{
	return _moves.size();
}

void			Board::switch_player(void)
{
	if (_currentPlayer != _PLAYER1 && _currentPlayer != _PLAYER2)
		throw Board::NoPlayer();
	else if (_currentPlayer == _PLAYER1)
	{
		_oppositePData = &_p1Data;
		_currentPData = &_p2Data;
	}
	else
	{
		_oppositePData = &_p2Data;
		_currentPData = &_p1Data;
	}
	_currentPlayer = switchPlayer(_currentPlayer);
}

PlayerData &	Board::getPlayerData(e_player player)
{
	if (player == _PLAYER1)
		return _p1Data;
	else
		return _p2Data;
}

const PlayerData &	Board::getPlayerData(e_player player) const
{
	if (player == _PLAYER1)
		return _p1Data;
	else
		return _p2Data;
}

PlayerData &	Board::currentPlayer(void)
{
	return *_currentPData;
}

PlayerData &	Board::oppositePlayer(void)
{
	return *_oppositePData;
}

const PlayerData & Board::currentPlayer(void) const
{
	return *_currentPData;
}

void			Board::_initSearchArea(void)
{
	_xarea = {AREA_SIZE, 0};
	_yarea = {AREA_SIZE, 0};
}

bool			Board::isInSearchArea(size_t pos)
{
	return (std::find(_area.begin(), _area.end(), pos) != _area.end());
}

const Area &			Board::getPlayableArea(void) const
{
	return _area;
}

void			Board::_update_search_area(int pos)
{
	int			p;
	Area &		area = _area;
	int px = pos % AREA_SIZE;
	int	xm = (px < 2 ? 3 + px : 5) + (px >= 16 ? 16 - px : 0);
	int py = pos / AREA_SIZE;
	int	ym = (py < 2 ? 3 + py : 5) + (py >= 16 ? 16 - py : 0);
	p = (py < 2 ? 0 : py - 2) * AREA_SIZE + (px < 2 ? 0 : px - 2);
	for (int i = 0 ; i < ym ; i++)
	{
		for (int j = 0 ; j < xm ; j++)
		{
			if (/*_board[ps].cell == _EMPTY && */!this->isInSearchArea(p + j))
				area.push_back(p + j);
		}
		p += AREA_SIZE;
	}
}

void			Board::_set(size_t pos, enum e_cell color)
{
	_board[pos].cell = color;
}

void			Board::set(size_t pos, enum e_cell color)
{
	_last = pos;
	_set(pos, color);
}

void			Board::_unset(size_t pos)
{
	_board[pos].cell = _EMPTY;
}

void			Board::unset(size_t pos)
{
	e_cell		current = _board[pos].cell;
	_unset(pos);
	_oppositePData->cellNb -= 1;
	supress_alignments(pos, current);
}

void			Board::_init_star(void)
{
	int		x;
	int		y;

	auto add_arm_to_star = [&] (int i, std::function<void (int, int)> fu) {
		_star[i].push_back(std::vector<Board::Node *> ());
		for (int j = -5 ; j < 6 ; j++) fu(i, j);
	};
	auto horizontal = [&] (int i, int j) {
		if (x + j >= 0 && x + j < AREA_SIZE)
			_star[i][0].push_back(&_board[i + j]);
	};
	auto vertical = [&] (int i, int j) {
		if (y + j >= 0 && y + j < AREA_SIZE)
			_star[i][1].push_back(&_board[i + j * 19]);
	};
	auto diagonalUpLeftToDownRight = [&] (int i, int j) {
		if (x + j >= 0 && y + j >= 0 && x + j < AREA_SIZE && y + j < AREA_SIZE)
			_star[i][2].push_back(&_board[i + j + j * 19]);
	};
	auto diagonalDownRightToUpLeft = [&] (int i, int j) {
		int	u = -j;
		if (x + j >= 0 && y + u >= 0 && x + j < AREA_SIZE && y + u < AREA_SIZE)
			_star[i][3].push_back(&_board[i + j + u * 19]);
	};

	for (int i = 0 ; i < CELL_NUMBER ; i++)
	{
		_star.push_back(std::vector<std::vector<Board::Node *> > ());
		x = i % 19;
		y = i / 19;
		add_arm_to_star(i, horizontal);
		add_arm_to_star(i, vertical);
		add_arm_to_star(i, diagonalUpLeftToDownRight);
		add_arm_to_star(i, diagonalDownRightToUpLeft);
	}
}

void			Board::_init_star_centers(void)
{
	for (int i = 0 ; i < CELL_NUMBER ; i++)
	{
		_starCenter.push_back(std::vector<int>());
		for (int j = 0 ; j < 4 ; j++)
		{
			for (size_t k = 0 ; k < _star[i][j].size() ; k++)
			{
				if (_star[i][j][k]->id == i)
					_starCenter[i].push_back(k);
			}
		}
	}
}

void			Board::print_playerData(PlayerData * playerData) //DEBUG
{
	std::cout << "Player: " << playerData-> id << std::endl;
	std::cout << "Color: " << (playerData->color == _WHITE ? "WHITE" : "BLACK")
		<< std::endl;
	std::cout << "Cell Nb: " << playerData->cellNb << std::endl;
	for (int i = 0 ; i < ALIGN_STORAGE_NB ; i++)
		std::cout << "free[" << i << "]: "
			<< playerData->freeAligneds[i] << std::endl;
	for (int i = 0 ; i < ALIGN_STORAGE_NB ; i++)
		std::cout << "one[" << i << "]: "
			<< playerData->oneAligneds[i] << std::endl;
	std::cout << "fiveAligns: " << playerData->fiveAligneds << std::endl;
	std::cout << "capturables: " << playerData->capturables << std::endl;
	std::cout << "captures: " << playerData->captures << std::endl;
	std::cout << "  =========   " << std::endl;
}

void		Board::print_move(const Move & move)
{
	std:: cout << " == MOVE == " << std::endl;
	std::cout << "id: " <<  move.pos << std::endl;
	std::cout << "captured:";
	auto    it = move.captures.begin();

	while (it != move.captures.end())
	{
		std::cout << " " << *it;
		it++;
	}

	auto ita = move.area.begin();
	while (ita != move.area.end())
	{
		std::cout << " " << *ita;
		ita++;
	}	
	std::cout << std::endl << std::endl;

}

const char *Board::NoPlayer::what ( void ) const noexcept
{
	return ("Board Error: no player set");
}
