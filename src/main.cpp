
#include <SFML/Graphics.hpp>
#include "Menu.class.hpp"
#include "UI/ui.hpp"

#define WIDTH	1024
#define	HEIGHT	768

int main(void)
{
	ui::Label::defaultFont.loadFromFile("./misc/king.ttf");
	ui::Banner::defaultTexture.loadFromFile("./misc/Banner.png");
	ui::Banner::defaultColor = sf::Color(230, 230, 230, 230);
	ui::Button::defaultTexture.loadFromFile("./misc/button.png");
	Menu menu(WIDTH, HEIGHT);
	ui::init(WIDTH, HEIGHT, "Gomoku");
	ui::Displayer::instance().getWindow().setFramerateLimit(30);
	ui::setMainArea(&menu);
	ui::loop();
	return 0;
}
