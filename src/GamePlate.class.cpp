#include "GamePlate.class.hpp"
#include "Game.class.hpp"

#define DEBUG_TH(x) std::cout << x << std::endl;

sf::Texture	GamePlate::boardButtonTexture;

ColorMap GamePlate::colors = {
	{_p1Color, sf::Color(80, 80, 80, 255)},
	{_p2Color, sf::Color(255, 255, 255, 255)},
	{_transparent, sf::Color::Transparent},
	{_forbidenColor, sf::Color(200, 0, 0, 128)},
	{_debugColor, sf::Color(0, 0, 0, 60)},
	{_green, sf::Color(0, 100, 0, 60)}
};

GamePlate::GamePlate(Game & game):
	Background(NULL),
	_slots(NULL),
	_game(game)
{
	_texture.loadFromFile("misc/Boardv1.png");
	this->setTexture(_texture);
}

GamePlate::~GamePlate(void)
{
	delete [] _slots;
	delete [] _debug;
}

void		GamePlate::init(void)
{
	static int	size = 19 * 19;
	float		btRatio = 1.0 / 18.2;
	this->boardButtonTexture.loadFromFile("misc/GameButton1.png");

	_slots = new ui::Button * [size];
	for (int i = 0 ; i < size ; i++)
	{
		_slots[i] = new ui::Button();
		_slots[i]->init();
		_slots[i]->getShape().setTexture(&this->boardButtonTexture);
		_slots[i]->getShape().setFillColor(sf::Color::Transparent);
		_slots[i]->setNormalColor(sf::Color::Transparent);
		_slots[i]->setActiveColor(sf::Color(0, 0, 100, 155));
		_slots[i]->setSelectedColor(sf::Color(0, 255, 0, 180));
		this->addSonArea(_slots[i]);
		_slots[i]->setBoundsFromParent(btRatio * (i % 19) - 0.5 * btRatio,
									   btRatio * (i / 19) - 0.5 * btRatio,
									   btRatio, btRatio);
		_slots[i]->setId(i);
		_slots[i]->setClickAction(UI_DELEGATE(&GamePlate::clickAction, this));
	}
	_debug = new ui::Background * [size];
	btRatio = 1.0 / 18.4;
	for (int i = 0 ; i < size ; i++)
	{
		_debug[i] = new ui::Background();
		_debug[i]->getShape().setFillColor(this->colors[_transparent]);
		this->addSonArea(_debug[i]);
		_debug[i]->setBoundsFromParent(-0.02 + btRatio * (i % 19),
									   -0.02 + btRatio * (i / 19),
									   btRatio, btRatio);
		_debug[i]->setId(i);
	}
}

void	GamePlate::activeIfColor(int buttonId, e_GameColor color)
{
	if (_slots[buttonId]->getShape().getFillColor() == this->colors[color])
	{
		this->activeCell(buttonId, _transparent);
	}
}

void	GamePlate::enableEvents(int i)
{
	_slots[i]->setListenEvents(true);
}

void	GamePlate::playTo(int buttonId, e_GameColor color)
{
	this->updateCellColor(buttonId, color);
	if (color == _p1Color || color == _p2Color)
	{
		_slots[buttonId]->setListenEvents(false);
	}
	if (this->isListening() && color == _transparent)
	{
		_slots[buttonId]->setListenEvents(true);
	}
}

void	GamePlate::updateCellColor(int buttonId, e_GameColor color)
{
	_slots[buttonId]->getShape().setFillColor(this->colors[color]);	
}

void	GamePlate::activeCell(int buttonId, e_GameColor color)
{
	this->updateCellColor(buttonId, color);
	_slots[buttonId]->setListenEvents(true);
}

void	GamePlate::showDebug(int id, e_GameColor color)
{
	_debug[id]->getShape().setFillColor(this->colors[color]);
}

void	GamePlate::disableCell(int buttonId, e_GameColor color)
{
	this->updateCellColor(buttonId, color);
	_slots[buttonId]->setListenEvents(false);
}

void	GamePlate::clickAction(void)
{
	_game.playTo(ui::EventManager::instance().getCurrentAreaId());
}
