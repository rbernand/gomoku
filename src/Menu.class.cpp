#include "Menu.class.hpp"

std::vector<std::string> Menu::_diffText = {"stupid", "bad", "easy",
										 "medium", "Hard", "insane"};

Menu::Menu(const float & width, const float & height)
	: Background(NULL),
	  _lastGame(NULL), _delGame(false), _stopReload(false)
{
	this->setSize(width, height);
	_texture.loadFromFile("./misc/Background.jpg");
	this->setTexture(_texture);

	_p1Depth = 3;
	_p2Depth = 3;
	
	ui::LabelDelegateMap buttons = {
		{"1 vs 1", UI_DELEGATE(&Menu::_1v1, this)},
		{"1 vs AI", UI_DELEGATE(&Menu::_1vAI, this)},
		{"AI vs AI", UI_DELEGATE(&Menu::_AIvAI, this)},
		{"Genetic Alogrythm", UI_DELEGATE(&Menu::_AIvAIGenetic, this)},
		{"exit", UI_DELEGATE(&Menu::_quit, this)}
	};
	_lst = ui::ButtonList::addVertical(this, buttons);

	buttons = {
		{"1", UI_DELEGATE(&Menu::_changeDifficultyp1, this)},
		{"2", UI_DELEGATE(&Menu::_changeDifficultyp1, this)},
		{"3", UI_DELEGATE(&Menu::_changeDifficultyp1, this)},
		{"4", UI_DELEGATE(&Menu::_changeDifficultyp1, this)},
		{"5", UI_DELEGATE(&Menu::_changeDifficultyp1, this)},
		{"6", UI_DELEGATE(&Menu::_changeDifficultyp1, this)},
	};
	_p1DiffButtons = ui::ButtonList::addVertical(this, buttons);
	_p1DiffButtons->setSizeFromParent(0.1, 0.8);
	_p1DiffButtons->setPositionFromParent(0.05, 0.1);
	buttons = {
		{"1", UI_DELEGATE(&Menu::_changeDifficultyp2, this)},
		{"2", UI_DELEGATE(&Menu::_changeDifficultyp2, this)},
		{"3", UI_DELEGATE(&Menu::_changeDifficultyp2, this)},
		{"4", UI_DELEGATE(&Menu::_changeDifficultyp2, this)},
		{"5", UI_DELEGATE(&Menu::_changeDifficultyp2, this)},
		{"6", UI_DELEGATE(&Menu::_changeDifficultyp2, this)},
	};
	_p2DiffButtons = ui::ButtonList::addVertical(this, buttons);
	_p2DiffButtons->setSizeFromParent(0.1, 0.8);
	_p2DiffButtons->setPositionFromParent(0.85, 0.1);
	for (unsigned int i = 0 ; i < _p1DiffButtons->getButtons().size() ; i++)
		_p1DiffButtons->getButtons()[i]->setId(i);
	for (unsigned int i = 0 ; i < _p2DiffButtons->getButtons().size() ; i++)
		_p2DiffButtons->getButtons()[i]->setId(i);
}

Menu::~Menu(void)
{

}

void		Menu::_updateButtonsText(void)
{
	_lst->getButtons()[1]->getLabel()->setString(
		"1 VS IA (" + _diffText[_p2Depth - 1] + ")");
	_lst->getButtons()[2]->getLabel()->setString(
		"IA (" + _diffText[_p1Depth - 1] + " ) VS IA ("
		+ _diffText[_p2Depth - 1] + ")");
	_lst->getButtons()[1]->getLabel()->center();
	_lst->getButtons()[2]->getLabel()->center();
}

void		Menu::_changeDifficultyp1(void)
{
	int id = ui::EventManager::instance().getCurrentAreaId();
	std::cout << "ID: " << id << std::endl;
	_p1Depth = id + 1;
	_updateButtonsText();
}

void		Menu::_changeDifficultyp2(void)
{
	int id = ui::EventManager::instance().getCurrentAreaId();
	_p2Depth = id + 1;
	_updateButtonsText();	
}

void		Menu::update(void)
{
	if (_delGame)
	{
		_lastGame->setActive(false);
		delete _lastGame;
		_lastGame = NULL;
		_delGame = false;
		if (!_stopReload)
		{
			_stopReload = false;
			_AIvAIGenetic();
		}
	}
}

void		Menu::delLastGame(bool stopReload)
{
	_delGame = true;
	_stopReload = stopReload;
}

void		Menu::_1v1(void)
{
	if (_lastGame)
		return;

	_lastGame = new Game();
	_lastGame->setReturnArea(this);
	ui::setMainArea(_lastGame);
	_lastGame->initDisplay();
}

void		Menu::_1vAI(void)
{
  	if (_lastGame)
		return;
	IA *	ai = new IA("Henry", _PLAYER2, _p2Depth);

	_lastGame = new Game(ai);
	_lastGame->setReturnArea(this);
	ui::setMainArea(_lastGame);
	_lastGame->initDisplay();
}


void		Menu::_AIvAI(void)
{
	if (_lastGame)
		return;
	IA *	ai1 = new IA("Sophy", _PLAYER1, _p1Depth);
	IA *	ai2 = new IA("Henry", _PLAYER2, _p2Depth);

	_lastGame = new Game(ai1, ai2);
	_lastGame->setReturnArea(this);
	ui::setMainArea(_lastGame);
	_lastGame->initDisplay();
}

void		Menu::_AIvAIGenetic(void)
{
	if (_lastGame)
		return;
	GeneticSolver &	gn = GeneticSolver::instance();
	IA *	ai1 = new IA("Sophy", _PLAYER1, _p1Depth, gn.getCurrentRatios(0));
	IA *	ai2 = new IA("Henry", _PLAYER2, _p2Depth, gn.getCurrentRatios(1));
	_lastGame = new Game(ai1, ai2, true);

	_lastGame->setReturnArea(this);
	ui::setMainArea(_lastGame);
	_lastGame->initDisplay();
}

void		Menu::_quit(void)
{
	ui::Displayer::instance().getWindow().close();
}
