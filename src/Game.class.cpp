#include "Game.class.hpp"
#include <Timeme.hpp>
#define DEBUG_TH(x) std::cout << x << std::endl;

Game::Game(void):
	Background(NULL),
	_board(*this),
	_p1(new Player("p1", _PLAYER1)),
	_p2(new Player("PLayer 2", _PLAYER2)),
	_current(NULL),
	_opposite(NULL),
	_menuButton(NULL),
	_cancelLastButton(NULL),
	_debugButton(NULL),
	_statusBanner(NULL),
	_gamePlate(NULL),
	_backTarget(NULL),
	_playing(false),
	_debug(false),
	_lastArea(),
	_last(-1),
	_moves(0),
	_clock(),
	_gameTimer(),
	_newTurn(false),
	_geneticSearch(false)
{
	_init_ui();
}

Game::Game(Player * player):
	Background(NULL),
	_board(*this),
	_p1(new Player("p1", _PLAYER1)),
	_p2(player),
	_menuButton(NULL),
	_cancelLastButton(NULL),
	_debugButton(NULL),
	_statusBanner(NULL),
	_gamePlate(NULL),
	_backTarget(NULL),
	_playing(false),
	_debug(false),
	_lastArea(),
	_last(-1),
	_moves(0),
	_clock(),
	_gameTimer(),
	_newTurn(false),
	_geneticSearch(false)
{
	_init_ui();
}

Game::Game(Player * player1, Player * player2, bool genSearch):
	Background(NULL),
	_board(*this),
	_p1(player1),
	_p2(player2),
	_menuButton(NULL),
	_cancelLastButton(NULL),
	_debugButton(NULL),
	_statusBanner(NULL),
	_gamePlate(NULL),
	_backTarget(NULL),
	_playing(false),
	_debug(false),
	_lastArea(),
	_last(-1),
	_moves(0),
	_clock(),
	_gameTimer(),
	_newTurn(false),
	_geneticSearch(genSearch)
{
	_init_ui();
}

Game::~Game(void)
{
	timeme::report();
	timeme::clear();
}

void	Game::_init_ui(void)
{
	_texture.loadFromFile("misc/woodPlate.jpg");
	this->setTexture(_texture);
	this->setSize(ui::Displayer::instance().getDefaultSize());
	_gamePlate = new GamePlate(*this);
	this->addSonArea(_gamePlate);
	_gamePlate->setBoundsFromParent(0.30, 0.14, 0.68, 0.84);
	_gamePlate->init();
	_menuButton = ui::Button::addDefault(this, "Menu");
	_menuButton->setBoundsFromParent(0.02, 0.02, 0.13, 0.10);
	_menuButton->setClickAction(UI_DELEGATE(&Game::_backToMenu, this));
	_cancelLastButton = ui::Button::addDefault(this, "Cancel");
	_cancelLastButton->setBoundsFromParent(0.82, 0.02, 0.14, 0.10);
	_cancelLastButton->setClickAction(UI_DELEGATE(&Game::cancelLastTurn, this));
	_debugButton = ui::Button::addDefault(this, "Debug");
	_debugButton->setBoundsFromParent(0.16, 0.02, 0.13, 0.10);
	_debugButton->setClickAction(UI_DELEGATE(&Game::_swapDebugMode, this));
	_statusBanner = ui::Banner::addDefault(this, "Game Loading");
	_statusBanner->setBoundsFromParent(0.3, 0.02, 0.5, 0.1);
	_statusBanner->getLabel()->setCharacterHeightFromParent(0.5);
	_statusBanner->getLabel()->horizontalCenter();
	this->addSonArea(_p1);
	_p1->setBoundsFromParent(0.020, 0.15, 0.26, 0.40);
	_p1->init_ui(_board.getPlayerData(_PLAYER1));
	this->addSonArea(_p2);
	_p2->setBoundsFromParent(0.020, 0.57, 0.26, 0.40);
	_p2->init_ui(_board.getPlayerData(_PLAYER2));
	_current = _p2;
// TODO set player color (first must be black/white)
	_board.setPlayer(_PLAYER1);
	_playing = true;
	_gameTimer.restart();
	this->playTurn();
}

void	Game::initDisplay(void)
{
	_p1->showDatas();
	_p2->showDatas();
}

void	Game::setReturnArea(ui::SystemArea * area)
{
	_backTarget = area;
}

// Method called each frame by ui library HF.
void	Game::update(void)
{
	static sf::Clock	clock;

	if (_newTurn == true)
	{
		this->playTurn();
		_newTurn = false;
	}
	if (_playing && clock.getElapsedTime() > sf::milliseconds(80))
	{
		_current->setTimer(_clock.getElapsedTime());
		clock.restart();
	}
}

void	Game::playTo(int buttonId)
{
	_moves += 1;
	_board.playTo(buttonId, _board.currentPlayer().color);
	_last = buttonId;
	_lastArea = _board.getPlayableArea();
	_showDebug(buttonId);
	this->endTurn();
}

void	Game::cancelLastTurn(void)
{
	DEBUG_TH("Dynamic: " << _current->getName());
	IA *	p;

	if (dynamic_cast<IA *>(_current) && dynamic_cast<IA *>(_opposite))
		return ; // Tant que pas de random // mettre une pause
	if ((p = dynamic_cast<IA *>(_current)))
	{
		DEBUG_TH("Before cancel");
		if (p->cancel() == true) // Evite le multicall sur le thread
		{
			if ((p = dynamic_cast<IA *>(_opposite)) == NULL)
			{
				_board.unplayLast();
				_board.switch_player();
			}
			_board.unplayLast();
			DEBUG_TH("CANCELLaTIONl");
			this->endTurn();
		}
		DEBUG_TH("CANCEL FINIDH");
	}
	else
	{
		DEBUG_TH("Not an IA ????");
		if ((p = dynamic_cast<IA *>(_opposite)))
		{
			DEBUG_TH("Cancel opposite");
			_board.unplayLast();
			_board.switch_player();
		}
		_board.unplayLast();
		this->endTurn();
	}
}

void	Game::updateCaptures(const PlayerData & player)
{
	Player * toUpdate;

	toUpdate = player.id == _PLAYER1 ? _p1 : _p2;
	toUpdate->updateCaptures(player.captures);
}

void	Game::updatePlate(int x, int y, e_cell newType)
{
	this->updatePlate(y * 19 + x, newType);
}

void	Game::updatePlate(int where, e_cell newType)
{
// TODO delete this stupid if forest.
	e_GameColor color;

	if (newType == _EMPTY)
	{
		_gamePlate->playTo(where, _transparent);
		return;
	}
	else if (newType == _BLACK)
		color = _p1Color;
	else if (newType == _WHITE)
		color = _p2Color;
	else
		color = _forbidenColor;
	_gamePlate->playTo(where, color);
}

// Here update when turn begin
void	Game::playTurn(void)
{
	Player *	current = this->getCurrentPlayerUi();

	_opposite = _current;
	_current = current;
	_statusBanner->setString(current->getName());
	_statusBanner->getLabel()->horizontalCenter();
	if (_board.has_win())
	{
		this->endGame();
		return;
	}
	current->play(*this);
}

void	Game::endGame(void)
{
	_save_game();
	std::stringstream	ss;

	ss << _current->getName() << " won in" << _moves << " moves"
	   << " and " << _gameTimer.getElapsedTime().asMilliseconds() << " ms";
	std::cout << ss.str() << std::endl;
	_statusBanner->setString(ss.str());
	_statusBanner->getLabel()->setWidthFromParent(0.9);
	_statusBanner->getLabel()->horizontalCenter();
	this->setListenEvents(false);
	_menuButton->setListenEvents(true);
	_debugButton->setListenEvents(true);
	_playing = false;
	if (_geneticSearch)
	{
		_systemDraw();
		this->setActive(false);
		ui::Displayer::instance().getWindow().display();
		GeneticSolver & gs = GeneticSolver::instance();
		std::vector<int> eval = {
			_current->getId() == 0,
			_moves, _gameTimer.getElapsedTime().asMilliseconds()
		};
		gs.evaluateCurrentState(eval, 0);
		eval[0] = _current->getId() == 1;
		gs.evaluateCurrentState(eval, 1);
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
		reinterpret_cast<Menu *>(this->_backTarget)->delLastGame(false);
		ui::setMainArea(_backTarget);
	}
}

void	Game::endTurn(void)
{
	this->getCurrentPlayerUi()->setTimer(_clock.restart());
	_p1->update_datas();
	_p2->update_datas();
	if (_board.has_win())
	{
		this->endGame();
		return;
	}
	_deleteForbiddens();
	_board.switch_player();
	_displayForbiddens();
	if (!dynamic_cast<IA *>(this->getCurrentPlayerUi()))
		this->enableBoardEvents();
	_newTurn = true;
}

void	Game::_save_game(void)
{
	std::stringstream			ss;
	std::ofstream				ofs;
	time_t						timer;
	std::vector<Board::Move>	moves = _board.getMoves();

	time(&timer);
	ss << "misc/Logs/" << timer << ".log";
	ofs.open(ss.str());
	for (auto it = moves.begin(); it != moves.end(); it++)
	{
		ofs << (*it).pos << std::endl;
	}
	ofs.close();
}

void	Game::_deleteForbiddens(void)
{
	for (int i = 0 ; i < CELL_NUMBER ; i++)
	{
		const Board::Node & nd = _board.getBoard()[i];
		if (nd.cell == _EMPTY)
			_gamePlate->activeCell(i, _transparent);
	}
}

void	Game::_displayForbiddens(void)
{
	e_cell current = _board.currentPlayer().color;
	const Board::Node *	board = _board.getBoard();
	
	for (int i = 0 ; i < CELL_NUMBER ; i++)
	{
		if (board[i].cell == _EMPTY && _board.countFreeThree(i, current) >= 2)
			_gamePlate->disableCell(i, _forbidenColor);
	}
}

void	Game::_hideDebug(void)
{
	for (int i = 0 ; i < CELL_NUMBER ; i++)
	{
		_gamePlate->showDebug(i, _transparent);
	}
}

void	Game::_showDebug(int lastPLayed)
{
	if (!_debug || lastPLayed < 0)
		return;
	_hideDebug();
	const Area & a = _lastArea;
	for (auto it = a.begin() ; it != a.end(); it++)
	{
		_gamePlate->showDebug(*it, _debugColor);
	}
	_gamePlate->showDebug(lastPLayed, _green);
}

void	Game::_backToMenu(void)
{
	if (_backTarget)
	{
		_p1->quit();
		_p2->quit();
		reinterpret_cast<Menu *>(this->_backTarget)->delLastGame(true);
		ui::setMainArea(_backTarget);
		this->setActive(false);
	}
}

void	Game::_swapDebugMode(void)
{
	if (_debug)
		_hideDebug();
	_debug = !_debug;
	if (_debug)
		_showDebug(_last);
}
	
void	Game::disableBoardEvents(void)
{
	_gamePlate->setListenEvents(false);
}

void	Game::enableBoardEvents(void)
{
	static int	size = 19 * 19;
	e_cell current = _board.currentPlayer().color;
	const Board::Node * board = _board.getBoard();

	for (int i = 0 ; i < size ; i++)
	{
		if (board[i].cell == _EMPTY
			&& _board.countFreeThree(i, current) < 2)
			_gamePlate->enableEvents(i);
	}
}

Player *	Game::getCurrentPlayerUi(void)
{
	if (_board.currentPlayer().id == _PLAYER1)
		return _p1;
	else
		return _p2;
}

Board &		Game::getBoard(void)
{
	return _board;
}
