#include "GeneticSolver.class.hpp"

GeneticSolver	GeneticSolver::_instance = GeneticSolver();

const int		GeneticSolver::_defaultRatios[RATIOSNB] =
{
	8, 4, 0, 4000, 3100, 18000, 250, 300, 800, 100, 5000, 3000, 0, 0,
	8, 4, 0, 4000, 3000, 18000, 250, 100, 810, 105, 5100, 3100, 0, 0
};

Array			GeneticSolver::_bestRatios = Array(0);

#define TEST(x) if (!(x)) std::cout << YELLOW << "NOT RUNNING" << RESET \
									<< std::endl;						\
	else std::cout << GREEN << "RUNNING" << RESET << std::endl
#define ECHO(x) std::cout << x << std::endl;

void	__echoDoubleArray__(std::vector<std::vector<int> > v)
{
	for (int i = 0 ; i < (int)v.size() ; i++)
	{
		std::cout << "v[i] ";
		for (int j = 0 ; j < (int)v[i].size() ; j++)
			std::cout << v[i][j] << " ";
		std::cout << std::endl;
	}
}

GeneticSolver::GeneticSolver(void) :
	_saver("misc/Genetics"),
	_evaluatedId{-1, -1},
	_idToTest(),
	_states{{0, 0, 1}},
	_population{{0}},
	_running(true)
{
	std::srand(std::time(NULL));
	_idToTest.reserve(POPULATION * GAMES_NB);
	_loadState();
	_loadPopulation();
}

GeneticSolver::~GeneticSolver(void)
{

}

GeneticSolver &	GeneticSolver::instance(void)
{
	return _instance;
}

bool			GeneticSolver::running(void) const
{
	return _running;
}

void			GeneticSolver::_createNewGeneration(void)
{
	static std::vector<float>	probs(POPULATION);
	static DoubleArray			newPop(POPULATION);
	float						total = 0;
	float						best = 0;
	float						tmp = 0;
	int							i;

	for (int i = 0 ; i < POPULATION ; i++)
	{
		probs[i] = _states[i + 1][1 + GAMES_NB * STATE_PER_GAME];
		total += probs[i];
		if (probs[i] > tmp)
		{
			best = i;
			tmp = probs[i];
		}
	}
	ECHO(YELLOW "BEST IS: " << best << std::endl);
//	_saveBestRatios(_population[best]); -> TODO WHEN ALGO FINE
	for (i = _saveBestParents(probs, newPop) ; i < POPULATION ; i++)
	{
		const Array & p1 = _population[_getRandomFromProbs(probs, total)];
		const Array & p2 = _population[_getRandomFromProbs(probs, total)];
		newPop[i] = _intermediateRecombination(p1, p2);
		if (p1 == p2)
			i--;
	}
	_saveNewGeneration(newPop);
}

int			GeneticSolver::_saveBestParents(std::vector<float> probs,
											DoubleArray & newPop)
{
	int		nb = POPULATION / KEEPRATIOS;
	float	tmp;
	int		best;

	nb = nb % 2 == 0 ? nb : nb + 1;
	ECHO(RED << "nb: " << nb << RESET);
	std::vector<int>	test;
	for (int k = 0 ; k < nb ; k++)
	{
		tmp = std::numeric_limits<float>::min();
		best = 0;
		for (unsigned int i = 0 ; i < probs.size() ; i++)
		{
			if (probs[i] >= tmp
				&& std::find(test.begin(), test.end(), i) == test.end())
			{
				tmp = probs[i];
				best = i;
			}
		}
		ECHO(RED "Best: " << best << "val: " << probs[best] <<  RESET);
		newPop[k] = _population[best];
		test.push_back(best);
	}
	return nb;
}

void		GeneticSolver::_saveNewGeneration(const DoubleArray & gen)
{
	try
	{
		_saver.copyFile(POP_FILE, SAVE_FOLDER + std::string(POP_FILE)
						+ std::to_string(_states[0][1]));
		_saver.save(gen, POP_FILE, SaveManager::doubleVectorIntToStr);
		_saver.copyFile(STATES_FILE, SAVE_FOLDER + std::string(STATES_FILE)
						+ std::to_string(_states[0][1]));
		_saver.copyFile(IDS_FILE, SAVE_FOLDER + std::string(IDS_FILE)
						+ std::to_string(_states[0][1]));
		_initIdToTest();
		_states.erase(_states.begin() + 1, _states.end());
		_initStates(POPULATION, _states[0][1] + 1);
		_saver.save(_idToTest, IDS_FILE, SaveManager::vectorIntToStr);
		_saver.save(_states, STATES_FILE, SaveManager::doubleVectorIntToStr);
		ECHO(BLUE "New generation started succefully" RESET);
	}
	catch (SaveManager::SaveError & e)
	{
		ECHO(YELLOW << e.what() << " during save of generation "
			 <<  _states[0][1] << RESET);
		_running = 1;
	}
}

Array		GeneticSolver::_intermediateRecombination(const Array & p1,
													  const Array & p2)
{
	Array	ret(RATIOSNB);

	for (int i = 0 ; i < RATIOSNB ; i++)
	{
		float a = (-250.0 + (std::rand() % 1500)) / 1000;
		ret[i] = p1[i] * a + p2[i] * (1 - a);
	}
	if (std::rand() % 1000 < MUTRATIO)
	{
		ECHO(YELLOW " MUTATION " RESET);
		ret[std::rand() % RATIOSNB] = std::rand() % RD_RANGE - RD_MAX;
		ret[std::rand() % RATIOSNB] = std::rand() % RD_RANGE - RD_MAX;
	}
	return ret;
}

int			GeneticSolver::_getRandomFromProbs(
	const std::vector<float> probs, const float & total)
{
	float	val;
	int		j;

	val = (std::rand() % RAND_MAX) * total / RAND_MAX;
	for (j = 0 ; j < POPULATION ; j++)
	{
		val -= probs[j];
		if (val <= 0)
			return j;
	}
	return j;
}

void		GeneticSolver::evaluateCurrentState(const Array & evaluation,
												const int x)
{
	if (!_running || x < 0 || x > 1 || _evaluatedId[x] < 0)
		return;
	_updateState(evaluation, _evaluatedId[x]);
	try
	{
		_saver.copyFile(STATES_FILE, SAVE_FOLDER + std::string("LastStates"));
		_saver.copyFile(IDS_FILE, SAVE_FOLDER + std::string("LastIds"));
		_saver.save(_states, STATES_FILE, SaveManager::doubleVectorIntToStr);
		_saver.save(_idToTest, IDS_FILE, SaveManager::vectorIntToStr);
		_evaluatedId[x] = -1;
		ECHO(GREEN "Succefully saved current state for " <<
			 _evaluatedId[x] << RESET);
	}
	catch (SaveManager::SaveError e)
	{
		ECHO(YELLOW << e.what() << " Evaluating " << x << RESET);
		_running = false;
	}
	ECHO("\033[35m Evaluation " << x << " ended: " << _states[0][0] << RESET);
	if (_states[0][0] >= POPULATION * GAMES_NB)
	{
		_createNewGeneration();
		_loadPopulation();
	}
}

void		GeneticSolver::_updateState(const Array & evaluation, const int id)
{
	Array &		current = _states[id + 1];
	const int	evNb = current[0];
	const int	pos = evNb * STATE_PER_GAME + 1;
	float		prob;

	std::copy(evaluation.begin(), evaluation.end(), current.begin() + pos);
	current[0] += 1;
	prob = (current[pos + 1] * 100.0 + current[pos + 2] / 800.0) * 0.3;
	prob = current[pos] ? 10000 - prob : prob;
	prob = prob < 0 ? 0 : prob;
	current[pos + STATE_PER_GAME - 1] = prob;
	if (current[0] == GAMES_NB)
	{
		prob = 0;
		for (int i = 0 ; i < GAMES_NB ; i++)
			prob += current[(i + 1) * STATE_PER_GAME];
		prob /= GAMES_NB;
		current[1 + GAMES_NB * STATE_PER_GAME] = prob;
	}
	_states[0][0] += 1;
	_states[0][3] = _idToTest.size();
}

void			GeneticSolver::_loadState(void)
{

	////// load ids properly here.

	if (!_running)
		return;
	try
	{
		_states[0] = _saver.loadAsIntVector(STATES_FILE,
											STATE_PER_GAME * GAMES_NB + 2);	
		ECHO(_states[0][3]);
		if (_states[0][3] != 0)
			_idToTest = _saver.loadAsIntVector(IDS_FILE, _states[0][3]);
	}
	catch (SaveManager::SaveError e)
	{
		ECHO(YELLOW << e.what() << " first step states loading" << RESET);
		_running = false;
		return;
	}
	if (_states[0][1] == 0)
	{
		_initIdToTest();
		_initStates(POPULATION, 0);
		_initPopulation(POPULATION);
	}		
	try
	{
		_states = _saver.loadAsDoubleIntVector(STATES_FILE,
											   STATE_PER_GAME * GAMES_NB + 2,
											   POPULATION + 1);
		ECHO(GREEN "Succefully loaded Genetic algorythm states" RESET);
	}
	catch (SaveManager::SaveError e)
	{
		ECHO(YELLOW << e.what() << " s2" << RESET);
		_running = false;
	}
}

void			GeneticSolver::_initStates(const int size, const int generation)
{
	const Array	zero = Array(STATE_PER_GAME * GAMES_NB + 2);

	for (int i = 0 ; i < size ; i++)
	{
		_states.push_back(zero);
	}
	_states[0][0] = 0;
	_states[0][1] = generation;
	_states[0][2] = POPULATION;
	_states[0][3] = _idToTest.size();
}

void			GeneticSolver::_loadPopulation(void)
{
	if (!_running)
		return;
	try
	{
		_population = _saver.loadAsDoubleIntVector(POP_FILE,
												   RATIOSNB, POPULATION);
		ECHO(GREEN "Succefully loaded population" << RESET);
	}
	catch (SaveManager::SaveError e)
	{
		ECHO(YELLOW << e.what() << " loading population" << RESET);
		_running = false;
	}
}

void			GeneticSolver::_initIdToTest(void)
{
	int		id = 0;
	for (int i = 0 ; i < POPULATION * GAMES_NB ; i++)
	{
		_idToTest.push_back(id);
		if ((i + 1) % GAMES_NB == 0)
			id++;
	}
	const int size = _idToTest.size();
	for (int i = 0 ; i < POPULATION * GAMES_NB * 6 ; i++)
	{
		const int id1 = std::rand() % size;
		const int id2 = std::rand() % size;
		const int tmp = _idToTest[id1];
		_idToTest[id1] = _idToTest[id2];
		_idToTest[id2] = tmp;
	}
	try
	{
		_saver.save(_idToTest, IDS_FILE, SaveManager::vectorIntToStr);
		ECHO(GREEN "Succefully init ids to test" RESET);
	}
	catch (SaveManager::SaveError e)
	{
		ECHO(YELLOW << e.what() << " init random ids " RESET);
		_running = false;
	}
}

void			GeneticSolver::_initPopulation(const int size)
{
	DoubleArray		population;
	Array			ratios(RATIOSNB);
	population.reserve(size);
	
	for (int i = 0 ; i < size ; i++)
	{
		for (int j = 0 ; j < RATIOSNB ; j++)
		{
			ratios[j] = std::rand() % RD_RANGE - RD_MAX;
		}
		population.push_back(ratios);
	}
	try
	{
		_saver.save(population, POP_FILE, SaveManager::doubleVectorIntToStr);
		_states[0][1] = 1;
		_saver.save(_states, STATES_FILE, SaveManager::doubleVectorIntToStr);
		ECHO(GREEN "Sucefully initialised search Population " << RESET);
	}
	catch (SaveManager::SaveError e)
	{
		ECHO(YELLOW << e.what() << " in population init" << RESET);
		_running = false;
	}
}

const int *	GeneticSolver::getCurrentRatios(const int x)
{
	if (!_running || x < 0 || x > 1 || _states[0][0] >= POPULATION * GAMES_NB)
	{
		if (_states[0][0] >= POPULATION)
			ECHO(YELLOW "Invalid population size, aborting search" RESET);
		if (x < 0 || x > 1)
			ECHO(YELLOW "Invalid players's ratios asked: " << x << RESET);
		_running = false;
		return _defaultRatios;
	}

	int id = _evaluatedId[x];
	if (id >= 0 && _states[id][0] < GAMES_NB)
	{
		ECHO(YELLOW "Current: " << id << RESET);
		return &_population[id][0];
	}
	else
	{
		if (!_idToTest.size())
		{
			_evaluatedId[x] = -1;
			ECHO(YELLOW "Current random: " << id << RESET);
			return &_population[std::rand() % POPULATION][0];
		}
		id = _idToTest.back();
		_idToTest.pop_back();
		_evaluatedId[x] = id;
		ECHO(YELLOW "Current: " << id << RESET);
		return &_population[id][0];
	}
}

void		GeneticSolver::_saveBestRatios(const Array & best)
{
	try
	{
		_saver.save(best, BESTS_FILE, SaveManager::vectorIntToStr);
		ECHO(GREEN "Succefully found better ratio at generation: "
			 << _states[0][1] << RESET);
	}
	catch (SaveManager::SaveError e)
	{
		ECHO(YELLOW << e.what() << " saving best ratio at generation: "
			 << _states[0][1] << RESET);
		_running = false;
	}
}

const int * GeneticSolver::getBestRatios(void)
{
	try
	{
		if (_bestRatios.size() != RATIOSNB)
			_bestRatios = _saver.loadAsIntVector(BESTS_FILE, RATIOSNB);
		ECHO("\033[34m" " Loaded best ratios " << RESET);
		return &_bestRatios[0];
	}
	catch (SaveManager::SaveError e)
	{
		ECHO("\033[34m Loaded default ratios for " << RESET);
		return _defaultRatios;
	}
}


const int *	GeneticSolver::getDefaultRatios(void) const
{
	return _defaultRatios;
}

