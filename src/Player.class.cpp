/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Player.class.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/11 18:13:30 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/25 15:29:29 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Player.class.hpp>
#include "Game.class.hpp"

Player::Player(const char * name, enum e_player id) :
	Background(NULL),
	_name(name),
	_id(id),
	_color(id == _PLAYER1 ? _BLACK : _WHITE),
	_dataScreen(NULL)
{
}

Player::~Player(void)
{
	delete [] _stolenPieces;
}

void				Player::play(Game & game)
{
	(void)game;
}

void				Player::init_ui(PlayerData & datas)
{
	int		height = this->getSize().y;
	float	colorHeight = height * 0.12;
	float	stolenHeight = height * 0.12;

	this->setTexture(ui::Banner::defaultTexture);
	_nameLabel = ui::Label::addDefault(this, _name);
	_nameLabel->setCharacterHeightFromParent(0.12);
	_nameLabel->setTopFromParent(0.2);
	_timer = ui::Label::addDefault(this, "Time: ");
	_timer->setCharacterHeightFromParent(0.12);
	_timer->setTopFromParent(0.4);
	_time = ui::Label::addDefault(this, "0.0000");
	_time->setCharacterHeightFromParent(0.12);
	_time->setPositionFromParent(0.5, 0.38);
	_pieceColor = new ui::Background();
	_pieceColor->setTexture(GamePlate::boardButtonTexture);
	this->addSonArea(_pieceColor);
	_pieceColor->setPositionFromParent(0.72, 0.17);
	_pieceColor->setSize(colorHeight, colorHeight);
	_pieceColor->getShape().setFillColor(GamePlate::colors[_color == _BLACK ?
														   _p2Color : _p1Color]);
	_stolenPieces = new ui::Background *[10];
	for (int i = 0 ; i < 10 ; i++)
	{
		_stolenPieces[i] = new Background();
		_stolenPieces[i]->setTexture(GamePlate::boardButtonTexture);
		this->addSonArea(_stolenPieces[i]);
		_stolenPieces[i]->setPositionFromParent(0.1 + i / 2 * 0.1255,
												0.56 + 0.13 * (i % 2));
		_stolenPieces[i]->setSize(stolenHeight, stolenHeight);
		_stolenPieces[i]->getShape().setFillColor(
			GamePlate::colors[_transparent]);
	}
	_dataScreen = new DataScreen();
	this->addSonArea(_dataScreen);
	_dataScreen->setBoundsFromParent(0, 0, 1, 1);
	_dataScreen->initUi(datas);
	_dataButton = ui::Button::addDefault(this, "V");
	_dataButton->setBoundsFromParent(0.77, 0.68, 0.14, 0.12);
	_dataButton->setClickAction(UI_DELEGATE(&Player::showDatas, this));
}

void				Player::quit(void) {}

void				Player::setTimer(sf::Time time)
{
	std::ostringstream ss;
	float	ftime = time.asMicroseconds();

	ftime /= 1000000;
	ss << ftime;
	std::string str(ss.str());
	_time->setString(str.substr(0, 6));
}

void				Player::updateCaptures(int total)
{
	int			nb = 0;
	sf::Color	color = GamePlate::colors[_color == _WHITE ? _p2Color : _p1Color];

	while (nb < 5)
	{
		if (nb >= total)
		{
			color = GamePlate::colors[_transparent];
		}
		_stolenPieces[nb * 2]->getShape().setFillColor(color);
		_stolenPieces[nb * 2 + 1]->getShape().setFillColor(color);
		nb++;
	}
}

void			Player::showDatas(void)
{
	_dataScreen->setActive(!_dataScreen->isActive());
}

void			Player::update_datas(void)
{
	_dataScreen->update_datas();
}

const std::string &	Player::getName(void) const
{
	return _name;
}

e_player		Player::getId(void) const
{
	return _id;
}

enum e_player					switchPlayer(enum e_player p)
{
	return (p == _PLAYER1 ? _PLAYER2 : _PLAYER1);
}
