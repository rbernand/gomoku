#include "DataScreen.class.hpp"

DataScreen::DataScreen(void) :
	_pawnsLabel(),
	_freeLabel(),
	_onesLabel(),
	_datas(NULL)
{
	
}

DataScreen::~DataScreen(void)
{
	
}

void		DataScreen::initUi(PlayerData & datas)
{
	_datas = &datas;
	this->setTexture(ui::Banner::defaultTexture);
	_pawnsLabel[0] = ui::Label::addDefault(this, "Pawns: ");
	_pawnsLabel[0]->setCharacterHeightFromParent(0.06);
	_pawnsLabel[0]->setTopFromParent(0.2);
	_pawnsLabel[1] = ui::Label::addDefault(this, "0");
	_pawnsLabel[1]->setCharacterHeightFromParent(0.06);
	_pawnsLabel[1]->setPositionFromParent(0.35, 0.185);
	_freeLabel[ALIGN_STORAGE_NB] = ui::Label::addDefault(this, "Free");
	_freeLabel[ALIGN_STORAGE_NB]->setCharacterHeightFromParent(0.06);
	_freeLabel[ALIGN_STORAGE_NB]->setPositionFromParent(0.1, 0.30);
	for (int i = 0 ; i < ALIGN_STORAGE_NB ; i++)
	{
		_freeLabel[i] = ui::Label::addDefault(this, " ");
		_freeLabel[i]->setCharacterHeightFromParent(0.06);
	}
	_onesLabel[ALIGN_STORAGE_NB] = ui::Label::addDefault(this, "Ones");
	_onesLabel[ALIGN_STORAGE_NB]->setCharacterHeightFromParent(0.06);
	_onesLabel[ALIGN_STORAGE_NB]->setPositionFromParent(0.35, 0.30);
	for (int i = 0 ; i < ALIGN_STORAGE_NB ; i++)
	{
		_onesLabel[i] = ui::Label::addDefault(this, " ");
		_onesLabel[i]->setCharacterHeightFromParent(0.06);
	}
	_fiveAlignsLabel[0] = ui::Label::addDefault(this, "5 aligned: ");
	_fiveAlignsLabel[0]->setCharacterHeightFromParent(0.06);
	_fiveAlignsLabel[0]->setTopFromParent(0.72);
	_fiveAlignsLabel[1] = ui::Label::addDefault(this, "0");
	_fiveAlignsLabel[1]->setCharacterHeightFromParent(0.06);
	_fiveAlignsLabel[1]->setPositionFromParent(0.45, 0.72);
	_capturablesLabel[0] = ui::Label::addDefault(this, "capturables: ");
	_capturablesLabel[0]->setCharacterHeightFromParent(0.06);
	_capturablesLabel[0]->setTopFromParent(0.82);
	_capturablesLabel[1] = ui::Label::addDefault(this, "0");
	_capturablesLabel[1]->setCharacterHeightFromParent(0.06);
	_capturablesLabel[1]->setPositionFromParent(0.55, 0.805);
	this->update_datas();
}

void		DataScreen::update_datas(void)
{
	std::ostringstream ss;

	ss << _datas->cellNb;
	_pawnsLabel[1]->setString(ss.str());
	ss.str("");
	ss << _datas->fiveAligneds;
	_fiveAlignsLabel[1]->setString(ss.str());
	ss.str("");
	ss << _datas->capturables;
	_capturablesLabel[1]->setString(ss.str());
	for (int i = 0 ; i < ALIGN_STORAGE_NB ; i++)
	{
		ss.str("");
		ss << i + 1 << "    " << _datas->freeAligneds[i];
		_freeLabel[i]->setString(ss.str());
		_freeLabel[i]->setTopFromParent(0.4 + i * 0.08);
		ss.str("");
		ss << i + 1 << "    " << _datas->oneAligneds[i];
		_onesLabel[i]->setString(ss.str());
		_onesLabel[i]->setPositionFromParent(0.35, 0.4 + i * 0.08);
	}
}
