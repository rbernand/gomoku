#include "PlayerData.struct.hpp"

PlayerData::PlayerData(e_player constId):
	id(constId),
	color(constId == _PLAYER1 ? _WHITE : _BLACK),
	cellNb(0),
	fiveAligneds(0),
	freeAligneds(),
	oneAligneds(),
	capturables(0),
	captures(0),
	fiveStorage(),
	lastTurn(false)
{
	std::memset(freeAligneds, 0, sizeof(int) * ALIGN_STORAGE_NB);
	std::memset(oneAligneds, 0, sizeof(int) * ALIGN_STORAGE_NB);
}

bool	PlayerData::is_five_align(int id, int branch)
{
	return (this->fiveStorage.find(id + branch * CELL_NUMBER) != fiveStorage.end());
}

void	PlayerData::store_five_align(int id, int branch)
{
	if (!this->is_five_align(id, branch))
	{
		fiveStorage[id + branch * CELL_NUMBER] = std::pair<int, int>(id, branch);
		fiveAligneds += 1;
	}
}

void	PlayerData::delete_five_align(int id, int branch)
{
	if (this->is_five_align(id, branch))
	{
		fiveStorage.erase(id + branch * CELL_NUMBER);
		fiveAligneds -= 1;
	}
}

void	PlayerData::print_five_aligns(void) const
{
	for (auto & val: fiveStorage)
	{
		auto & x = val.second;
		std::cout << x.first << " - " << x.second << ", " << std::endl;
	}
}
