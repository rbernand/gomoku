#ifndef GENETICSOLVER_CLASS_HPP
# define GENETICSOLVER_CLASS_HPP

#include <cstdlib>
#include <ctime>
#include <cstring>
#include <algorithm>
#include "SaveManager.class.hpp"

#define STATES_FILE	"states"
#define IDS_FILE	"ids"
#define POP_FILE	"population"
#define BESTS_FILE	POP_FILE
#define SAVE_FOLDER	"Saves/"

#define RATIOSNB		28
#define RD_RANGE		30000
#define RD_MAX			14999
#define POPULATION		100

#define GAMES_NB		3
#define STATE_PER_GAME	4

#define CROSSRATIO		900
#define MUTRATIO		60
#define KEEPRATIOS	1.8

typedef	std::vector<std::vector<int> >	DoubleArray;
typedef	std::vector<int>				Array;

class GeneticSolver
{
public:
	static GeneticSolver &	instance();

	const int *		getBestRatios(void);
	const int *		getCurrentRatios(const int x);
	const int *		getDefaultRatios(void) const;
	void			evaluateCurrentState(const std::vector<int> & evaluation,
										 const int x);
	bool            running(void) const;
	
private:
	GeneticSolver();
	GeneticSolver(const GeneticSolver & src);
	~GeneticSolver();

	Array			_intermediateRecombination(const Array & p1,
											   const Array & p2);
	int				_getRandomFromProbs(const std::vector<float> probs,
										const float & total);
	void            _initPopulation(const int size);
	void			_initStates(const int size, const int generation);
	void			_initIdToTest(void);
	void			_updateState(const Array & evaluation, const int id);
	void			_loadState(void);
	void			_loadPopulation(void);
	void			_createNewGeneration(void);
	void			_saveBestRatios(const Array & best);
	int				_saveBestParents(std::vector<float> probs,
									 DoubleArray & best);
	void			_saveNewGeneration(const DoubleArray & gen);

	SaveManager		_saver;
	int				_evaluatedId[2];
	Array			_idToTest;
	DoubleArray		_states;
	DoubleArray		_population;
	bool			_running;

	static GeneticSolver		_instance;
	static const int			_defaultRatios[RATIOSNB];
	static Array				_bestRatios;
};

#endif

