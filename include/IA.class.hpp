/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IA.class.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/11 18:48:30 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/26 11:09:11 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IA_CLASS_HPP
# define IA_CLASS_HPP

# include <Player.class.hpp>
# include <thread>
# include <chrono>
# include <mutex>
# include "GeneticSolver.class.hpp"
# include <set>
#include <cstring>

#define MAX(X, Y)			X >= Y ? X : Y
#define MIN(X, Y)			X < Y ? X : Y
#define MIN_INT				-2147483648
#define MAX_INT				2147483647
# define WIN_VALUE			90000000

class Game;

struct		s_data_heur
{
	s_data_heur(unsigned int *seria, int dheur, enum e_player dplayer) {
		std::memcpy(this->serie, seria, sizeof(int) * 23);
		this->heur = dheur;
		this->player = dplayer;
	}

	s_data_heur(void) {
	}

	unsigned int				serie[23];
	int				heur;
	enum e_player	player;
};

class IA : public Player
{
public:
	IA(const char *name, enum e_player, size_t depth);
	IA(const char *name, enum e_player, size_t depth, const int * ratios);
	~IA(void);
	
	void			play(Game & game);
	void			update(void);
	void			quit(void);
	bool			cancel(void);
	bool			ready(void);
	int		heuristic(const Board & board, const PlayerData & playerDatas);

private:
	std::set<struct s_data_heur, bool (*)(struct s_data_heur, struct s_data_heur)>		_heuristic_values;
	size_t								_depth;
	size_t								_idToPlay;
	size_t			_ct;
	size_t			_cut;
	std::thread		_turn;
	bool			_done;
	std::mutex		_cancel;
	const int	*	_hRatios;
	
	void		_launchPlay(Game & game);
	void		_shell_sort(int tab[CELL_NUMBER][2], int ct);
	int			_getHeuristics(Board & board, const PlayerData & current,
							   int tab[CELL_NUMBER][2]);
	int			_minimax(Game & game, Board & board, int depth);
	int			_alphabeta(Game & game, Board & board, int depth, int alpha, int beta);
};

#endif
