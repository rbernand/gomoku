#ifndef PLAYERDATA_STRUCT_HPP
# define PLAYERDATA_STRUCT_HPP

#include <cstring>
#include <map>
#include <iostream>
#include <vector>

# define CELL_NUMBER		361

enum		e_player : char
{
	_NONE = -1,
	_PLAYER1,
	_PLAYER2,
	_MAX_PLAYER
};

// TODO _WHITE & _BLACK MUST BE 0-1 or 1-0 NOT NORMAL, find better.
enum		e_cell : char
{
	_WHITE = 0,
	_BLACK,
	_EMPTY
};

# define ALIGN_STORAGE_NB	4

typedef	std::map<int, std::pair<int, int>>	FiveAlign;

struct	PlayerData
{
public:
	PlayerData(enum e_player constId);

	bool	is_five_align(int id, int branch);
	void	store_five_align(int id, int branch);
	void	delete_five_align(int id, int branch);
	void	print_five_aligns(void) const;

	enum e_player	id;
	enum e_cell		color;
	int				cellNb;
	int				fiveAligneds;
	int				freeAligneds[ALIGN_STORAGE_NB];
	int				oneAligneds[ALIGN_STORAGE_NB];
	int				capturables;
	int				captures;
	FiveAlign		fiveStorage;
	bool			lastTurn;

private:
};

#endif
