#ifndef MENU_CLASS_HPP
# define MENU_CLASS_HPP

#include "UI/ui.hpp"
#include "Game.class.hpp"

#define ECHO(x)		std::cout << x << std::endl;

class Menu : public ui::Background
{
public:
	Menu(const float & width, const float & height);
	~Menu(void);

	void		update(void);
	void		delLastGame(bool stopReload);

private:
	Menu(void);
	Menu(const Menu & src);
	Menu & operator=(const Menu & rhs);

	void	_1vAI(void);
	void	_AIvAI(void);
	void	_AIvAIGenetic(void);
	void	_1v1(void);
	void	_quit(void);
	void	_changeDifficultyp1(void);
	void	_changeDifficultyp2(void);
	void	_updateButtonsText(void);

	Game *				_lastGame;
	bool				_delGame;
	bool				_stopReload;
	int					_p1Depth;
	int					_p2Depth;
	static std::vector<std::string>	_diffText;
	ui::ButtonList *	_lst;
	ui::ButtonList *	_p1DiffButtons;
	ui::ButtonList *	_p2DiffButtons;
};

#endif /* MENU_CLASS_HPP */
