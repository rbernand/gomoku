/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Board.class.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/11 14:09:36 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/28 13:22:57 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BOARD_CLASS_HPP
# define BOARD_CLASS_HPP

#include <iostream>
#include <cstring>
#include <vector>
#include <unordered_set>
#include <algorithm>
#include "PlayerData.struct.hpp"

# define AREA_SIZE			19

enum		e_dir
{
	_VERT = 0,
	_HORI,
	_NWSE,
	_NESW,
	_MAX_DIR
};

typedef std::pair<int, std::vector<int>>	Move;
typedef	std::vector<int>					Area;


class Game;
class Board
{
public:
	Board (Game & game);
	Board &			operator=(const Board &);

	~Board(void);

	struct	Node
	{
		int				id;
		enum e_cell		cell;
	};

	struct	Coord
	{
		int x;
		int y;
	};
	
	struct	Move
	{
		Move(int p, std::vector<int> c, Area & a,
			 PlayerData & p1, PlayerData & p2);

		int					pos;
		std::vector<int>	captures;
		Area				area;
		PlayerData			p1Data;
		PlayerData			p2Data;
	};

	unsigned int	*serialize(void) const;
	unsigned int	*serialize_at(size_t where, enum e_cell who);
	void			playTo(size_t where, enum e_cell who);
	void			unplayLast(void);
	bool			canPlayTo(size_t where, enum e_cell who);
	inline bool	isInBoard(int pos);
	bool			isInSearchArea(size_t pos);
	void			silentPlayTo(size_t where, enum e_cell who);
	void				silentUnplayLast(void);

	int				capture(size_t where);
	void			capture_void(size_t where);
	void            supress_alignments(size_t pos, e_cell supressed);
	void            update_alignments(size_t pos);
	int				countFreeThree(int id, e_cell current);
	/* void			updateDoubleThree(const int & id, e_cell current); */
	void			setPlayer(enum e_player player);
	void			switch_player(void);
	PlayerData &	currentPlayer(void);
	PlayerData &	oppositePlayer(void);
	PlayerData &	getPlayerData(enum e_player player);
	const PlayerData &	getPlayerData(enum e_player player) const;
	const PlayerData &	currentPlayer(void) const;
	const Area &	getPlayableArea(void) const;

	const Board::Node *	getBoard(void) const;
	enum e_cell		get (size_t) const;
	enum e_cell		get (size_t, size_t) const;
	void			set (size_t, enum e_cell);
	void			set (size_t, size_t, enum e_cell);
	void			unset (size_t);
	void			print_playerData(PlayerData * player);
	void			print_star (size_t where);
	void			print_move(const Move & move);

	bool			has_win (void) const;
	bool			has_win (const PlayerData & playerDatas) const;
	size_t			getLast (void) const;
	int				getMovesNb(void) const;
	std::vector<Move>	getMoves(void) const;

	typedef std::vector<std::vector<std::vector <Board::Node *> > >	ResStar;
	typedef std::vector<std::vector <Board::Node *> >				Star;
	typedef std::vector <Board::Node *>								Branch;
	typedef std::vector<std::vector <int> >							StarCenters;
	typedef std::vector <int>										Centers;
	
	class NoPlayer : public std::exception
	{
	public:
		const char *what (void) const noexcept;
	};
private:
	Board (void);
	Board (const Board & src);

	void			_init_star(void);
	void			_init_star_centers(void);

	void			_store_five_aligns(int pos, int branch, PlayerData & datas);
	void			_delete_five_aligns(int pos, int branch, PlayerData & datas);

	int				_align_count_left(int center, const Branch & branch,
									  e_cell current);
	int				_align_count_right(int center, const Branch & branch,
									   e_cell current);
	void			_align_update_value(int x, int sub, int nb, int * tab);
	void			_update_oneAligned(int x, int nb, PlayerData & datas,
									   int border, int max);
	bool			_five_align_breakable(void) const;
	bool			_can_be_captured(int id) const;

	void			_initSearchArea(void);
	void			_update_search_area(int pos);

	
	void			_set(size_t, enum e_cell);
	void			_unset(size_t);

	Board::Node		_board[CELL_NUMBER];
	Game &			_game;
	enum e_player	_currentPlayer;

	PlayerData		_p1Data;
	PlayerData		_p2Data;
	PlayerData *	_currentPData;
	PlayerData *	_oppositePData;

	ResStar			_star;
	StarCenters		_starCenter;

	size_t				_last;
	std::vector<int>	_lastCaptured;
	Board::Coord		_xarea;
	Board::Coord		_yarea;
	Area				_area;
	std::vector<Move>	_moves;
};

#endif
