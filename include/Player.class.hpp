/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Player.clas.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/11 18:13:28 by rbernand          #+#    #+#             */
//   Updated: 2015/10/14 17:50:52 by caupetit         ###   ########.fr       //
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER_CLASS_HPP
# define PLAYER_CLASS_HPP

#include <iostream>
#include <UI/ui.hpp>
#include <Board.class.hpp>
#include <sstream>
#include "GamePlate.class.hpp"
#include "PlayerData.struct.hpp"
#include "DataScreen.class.hpp"

class Game;

class Player : public ui::Background
{
	public:
		Player(const char *, enum e_player id);
		~Player();

		virtual void			play(Game & game);
		virtual void			init_ui(PlayerData & datas);
		virtual void			quit(void);

		void					showDatas(void);
		void					updateCaptures(int total);
		void					update_datas(void);
		void					setTimer(sf::Time time);

		const std::string &		getName(void) const;
		e_player				getId(void) const;
	private:
		const std::string		_name;
	protected:
		const enum e_player		_id;
		const enum e_cell		_color;

		Game *					_game;
		ui::Label *				_nameLabel;
		ui::Label *				_timer;
		ui::Label *				_time;
		ui::Background *		_pieceColor;
		ui::Background **		_stolenPieces;

		ui::Button *			_dataButton;
		DataScreen *			_dataScreen;
};

enum e_player					switchPlayer(enum e_player);

#endif
