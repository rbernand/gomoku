#ifndef GAME_CLASS_HPP
# define GAME_CLASS_HPP

#include <sstream>
#include "UI/ui.hpp"
#include "Player.class.hpp"
#include "IA.class.hpp"
#include "GamePlate.class.hpp"
#include "Menu.class.hpp"

class Game : public ui::Background
{
public:
	Game(void);
	Game(Player * player);
	Game(Player * player1, Player * player2, bool genSearch = false);
	~Game(void);

	void	initDisplay(void);
	void	update(void);
	void	playTo(int buttonId);
	void	updateCaptures(const PlayerData & player);
	void	updatePlate(int x, int y, e_cell newType);
	void	updatePlate(int where, e_cell newType);
	void	playTurn(void);
	void	cancelLastTurn(void);
	void	disableBoardEvents(void);
	void	enableBoardEvents(void);
	void	endTurn(void);
	void	endGame(void);
	void	setReturnArea(ui::SystemArea * area);

	Player *		getCurrentPlayerUi(void);
	Board &			getBoard(void);
	
private:
	Game(const Game & src);
	Game & operator=(const Game & rhs);

	void	_init_ui(void);

	void    _hideDebug(void);
	void	_showDebug(int lastPlayed);
	void	_deleteForbiddens(void);
	void	_displayForbiddens(void);

	void    _swapDebugMode(void);
	void	_backToMenu(void);
	void	_save_game(void);

	Board				_board;
	Player *			_p1;
	Player *			_p2;
	Player *			_current;
	Player *			_opposite;

	ui::Button *		_menuButton;
	ui::Button *		_cancelLastButton;
	ui::Button *		_debugButton;
	ui::Banner *		_statusBanner;
	ui::Background *	_player1Slot;
	ui::Background *	_player2Slot;
	GamePlate *			_gamePlate;
	ui::SystemArea *	_backTarget;
	
	bool				_playing;
	bool				_debug;
	Area				_lastArea;
	int					_last;
	int					_moves;
	sf::Clock			_clock;
	sf::Clock			_gameTimer;
	bool				_newTurn;
	bool				_geneticSearch;

};

#endif
