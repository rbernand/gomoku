#ifndef SAVE_MANAGER_CLASS_HPP
# define SAVE_MANAGER_CLASS_HPP

#define RED		"\033[31m"
#define GREEN	"\033[32m"
#define YELLOW	"\033[33m"
#define BLUE	"\033[34m"
#define	RESET	"\033[0m"

#include <string>
#include <cstring>
#include <iterator>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>

class	SaveManager
{
public:
	class SaveError : std::exception
	{
	public:
		SaveError(const std::string & err);
		const char *	what(void) const noexcept;
	private:
		const std::string	_error;
	};

	SaveManager(const std::string & file);
	~SaveManager(void);

	void	copyFile(const std::string & src, const std::string & dst);

	template <typename T>
	void	save(T data, std::string file, std::string (*dataToString)(T)) {
		std::ofstream	ofs(_saveRepository + "/" + file, std::ios::trunc);
		if (!ofs.is_open())
			throw SaveError(_saveRepository + "/" + file
							+ ": " + std::strerror(errno));
		ofs << dataToString(data);
		ofs.close();
	}

	template<typename T>
	void	save(T data, std::string (*dataTostring)(T)) {
		this->save(data, "Test", dataTostring);
	}

	std::vector<int>				loadAsIntVector(const std::string & file,
													const unsigned int len);
	std::vector<std::vector<int> >	loadAsDoubleIntVector(const std::string & f,
														  const unsigned int witdh,
														  const unsigned int height);

	static std::string		vectorIntToStr(std::vector<int> v);
	static std::string		doubleVectorIntToStr(std::vector<std::vector<int> > v);
	
private:
	SaveManager(void);
	SaveManager(const SaveManager & src);

	std::string		_saveRepository;
};

#endif
