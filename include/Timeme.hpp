
#ifndef TIMEME_HPP
# define TIMEME_HPP
# define START			timeme::start(__FUNCTION__);
# define END			timeme::end(__FUNCTION__);

namespace timeme {
	void			start(const char * fct);
	void			end(const char * fct);
	void			report(void);
	void			clear(void);
};
#endif
