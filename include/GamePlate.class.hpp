#ifndef GAMEPLATE_CLASS_HPP
# define GAMEPLATE_CLASS_HPP

#include "UI/ui.hpp"
#include <map>

enum	e_GameColor	
{
	_transparent = 0,
	_p1Color,
	_p2Color,
	_forbidenColor,
	_debugColor,
	_green,
	_ColorsNb
};

class Game;

typedef std::map<e_GameColor, sf::Color> ColorMap;

class GamePlate : public ui::Background
{
public:
	GamePlate(Game & game);
	~GamePlate(void);

	void		init(void);
	void		playTo(int buttonId, e_GameColor color);
	void		updateCellColor(int buttonId, e_GameColor color);
	void		enableEvents(int i);

	void		activeCell(int buttonId, e_GameColor color);
	void		disableCell(int buttonId, e_GameColor color);
	void		activeIfColor(int buttonId, e_GameColor color);

	void		showDebug(int id, e_GameColor color);
	
	void		clickAction(void);

	static ColorMap		colors;
	static sf::Texture	boardButtonTexture;

private:
	sf::Texture			_boardButtonTexture;
	ui::Button **		_slots;
	ui::Background **	_debug;
	Game &				_game;

};

#endif
