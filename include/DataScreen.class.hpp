#ifndef DATASCREEN_CLASS_HPP
# define DATASCREEN_CLASS_HPP

#include <UI/ui.hpp>
#include <sstream>
#include "PlayerData.struct.hpp"

class DataScreen : public ui::Background
{
public:
	DataScreen(void);
	~DataScreen(void);

	void		initUi(PlayerData & datas);
	void		update_datas(void);

private:
	ui::Label *				_pawnsLabel[2];
	ui::Label *				_freeLabel[ALIGN_STORAGE_NB + 1];
	ui::Label *				_onesLabel[ALIGN_STORAGE_NB + 1];
	ui::Label *				_fiveAlignsLabel[2];
	ui::Label *				_capturablesLabel[2];

	PlayerData *			_datas;
};

# endif
