NAME=gomoku
CC=c++
CFLAGS=-Wall -Werror -Wextra -std=c++11 -g -O3
DIR_BIN=bin/
DIR_INCLUDE=include/
DIR_OBJ=obj/
DIR_SRC=src/

SRC=main.cpp \
	Menu.class.cpp \
	Game.class.cpp \
	GamePlate.class.cpp \
	Board.class.cpp \
	PlayerData.struct.cpp \
	Player.class.cpp \
	DataScreen.class.cpp \
	IA.class.cpp \
	GeneticSolver.class.cpp \
	SaveManager.class.cpp \
	Timeme.cpp
OBJ=$(SRC:%.cpp=$(DIR_OBJ)%.o)

all: init $(NAME)

init: load_libs $(DIR_BIN) $(DIR_OBJ)


$(DIR_BIN):
	@mkdir -p $(DIR_BIN)

$(DIR_OBJ):
	@mkdir -p $(DIR_OBJ)

# ================ Extern libraries ================= #

# ==== >> UI Submodule << ======= #
DIR_UI = ui
UI_LIBS = -L$(DIR_UI)/lib -lui
UI_INCLUDE = $(DIR_UI)/include

$(DIR_UI)/include:
	git submodule init
	git submodule update

# =============================== #


# === >> SFML ui Submodule << === #
DIR_SFML = $(DIR_UI)/SFML
SFML_LIBS = -L$(DIR_SFML)/lib -lsfml-graphics -lsfml-window -lsfml-system
SFML_INCLUDE = $(DIR_SFML)/include

$(DIR_SFML)/include:
	git submodule update --init --recursive

# =============================== #

OS:= $(shell uname)
ifeq ($(OS), Darwin)
	OSX_SHARED_LIBS_LINK =	-rpath `pwd`/$(DIR_SFML)/extlibs/libs-osx/Frameworks \
							-rpath `pwd`/$(DIR_SFML)/lib \
							-rpath `pwd`/$(DIR_UI)/lib
	SHARED_SUFFIX = dylib
else ifeq ($(OS), Linux)
	SHARED_SUFFIX = so
	OSX_SHARED_LIBS_LINK =	-Wl,-rpath `pwd`/$(DIR_SFML)/lib \
							-Wl,-rpath `pwd`/$(DIR_UI)/lib
endif

$(DIR_SFML)/lib/libsfml-system.$(SHARED_SUFFIX):
	cd $(DIR_SFML) && cmake . && make
$(DIR_UI)/lib/libui.$(SHARED_SUFFIX):
	cd $(DIR_UI) && cmake . && make

OSX_SHARED_LIBS_LINK += 

load_libs:	$(DIR_UI)/include \
			$(DIR_SFML)/include \
			$(DIR_SFML)/lib/libsfml-system.$(SHARED_SUFFIX) \
			$(DIR_UI)/lib/libui.$(SHARED_SUFFIX)

# =================================================== #

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) -o $(DIR_BIN)$@ $^ $(UI_LIBS) $(SFML_LIBS) $(OSX_SHARED_LIBS_LINK)
	ln -f $(DIR_BIN)$@ $@

ref: $(OBJ)
	$(CC) $(CFLAGS) -o $(DIR_BIN)$@ $^ $(UI_LIBS) $(SFML_LIBS) $(OSX_SHARED_LIBS_LINK)
	ln -f $(DIR_BIN)$@ $@

$(DIR_OBJ)%.o: $(DIR_SRC)%.cpp
	$(CC) $(CFLAGS) -o $@ -c $< -I$(DIR_INCLUDE) -I$(SFML_INCLUDE) -I$(UI_INCLUDE)

clean:
	rm -f $(OBJ)
	rm -f $(NAME)

fclean: clean
	rm -rf $(DIR_BIN)
	rm -rf $(DIR_OBJ)

re: fclean all
